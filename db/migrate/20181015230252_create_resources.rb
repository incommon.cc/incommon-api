class CreateResources < ActiveRecord::Migration[5.2]
  def change
    create_table :resources do |t|
      t.string :name, limit: 80
      t.string :summary, limit: 136
      t.text :description
      t.json :meta
      t.boolean :public
      t.uuid :uuid
      t.string :type, limit: 16

      t.timestamps
    end
  end
end

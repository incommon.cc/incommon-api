class CreateResourceAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_addresses do |t|
      t.belongs_to :address, foreign_key: true
      t.belongs_to :resource, polymorphic: true
      t.integer :flags

      t.timestamps
    end
  end
end

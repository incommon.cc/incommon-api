class CreateResourceSections < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_sections do |t|
      t.references :resource, polymorphic: true
      t.references :section, foreign_key: true
    end
  end
end

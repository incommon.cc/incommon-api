class AddConstraintOnRoles < ActiveRecord::Migration[5.2]
  def change
    add_index :roles, [:user_id, :agent_id], unique: true
  end
end

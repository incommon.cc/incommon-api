class AddSha256HashToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :sha256_hash, :string, limit: 64
  end
end

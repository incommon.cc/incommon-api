class CreateResourceEmails < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_emails do |t|
      t.belongs_to :email, foreign_key: true
      t.belongs_to :resource, polymorphic: true
      t.integer :flags

      t.timestamps
    end
  end
end

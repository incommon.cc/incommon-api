class CreateResourceCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_collections do |t|
      t.belongs_to :resource, foreign_key: true
      t.belongs_to :collection, foreign_key: true

      t.timestamps
    end
  end
end

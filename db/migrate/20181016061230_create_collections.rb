class CreateCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :collections do |t|
      t.string :name, limit: 80, null: false
      t.string :summary, limit: 136
      t.text :description
      t.belongs_to :agent, index: true

      t.timestamps
    end
  end
end

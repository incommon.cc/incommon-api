class LiftLimitOnUsername < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :username, :string, limit: nil, unique: true
  end
end

class CreateMaps < ActiveRecord::Migration[5.2]
  def change
    create_table :maps do |t|
      t.integer :agent_id
      t.belongs_to :taxonomy, foreign_key: true
      t.belongs_to :position, foreign_key: true
      t.belongs_to :collection, foreign_key: true
      t.integer :zoom, limit: 2, default: 13
      t.uuid :uuid

      t.timestamps
    end
    add_index :maps, :uuid, unique: true
  end
end

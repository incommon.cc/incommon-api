class AddAgentToTaxonomy < ActiveRecord::Migration[5.2]
  def change
    add_column :taxonomies, :agent_id, :bigint, limit: 8
    add_index :taxonomies, :agent_id
  end
end

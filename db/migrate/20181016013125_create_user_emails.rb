class CreateUserEmails < ActiveRecord::Migration[5.2]
  def change
    create_table :user_emails do |t|
      t.belongs_to :email, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.integer :flags

      t.timestamps
    end
  end
end

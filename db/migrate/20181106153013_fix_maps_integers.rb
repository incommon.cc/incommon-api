class FixMapsIntegers < ActiveRecord::Migration[5.2]
  def change
    change_column :maps, :agent_id, :bigint
    change_column :positions, :radius, :integer
  end
end

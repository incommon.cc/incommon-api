class ChangeResourcesNameLength < ActiveRecord::Migration[5.2]
  def change
    change_column :resources, :name, :string, limit: 128
  end
end

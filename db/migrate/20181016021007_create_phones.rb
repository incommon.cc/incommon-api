class CreatePhones < ActiveRecord::Migration[5.2]
  def change
    create_table :phones do |t|
      t.string :number, limit: 32
      t.integer :flags

      t.timestamps
    end
    add_index :phones, :number, unique: true
  end
end

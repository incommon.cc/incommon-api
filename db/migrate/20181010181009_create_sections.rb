class CreateSections < ActiveRecord::Migration[5.2]
  def change
    create_table :sections do |t|
      t.string :name, limit: 64
      t.string :summary, limit: 136
      t.text :description
      t.references :category, foreign_key: true
      t.string :color, limit: 25

      t.timestamps
    end
  end
end

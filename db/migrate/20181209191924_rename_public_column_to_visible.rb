class RenamePublicColumnToVisible < ActiveRecord::Migration[5.2]
  def change
    change_table :resources do |t|
      t.rename :public, :visible
    end
  end
end

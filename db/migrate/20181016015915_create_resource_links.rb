class CreateResourceLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_links do |t|
      t.references :link, foreign_key: true
      t.references :resource, polymorphic: true
      t.integer :flags

      t.timestamps
    end
  end
end

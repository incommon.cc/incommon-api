class FixResourcesCountOnSections < ActiveRecord::Migration[5.2]
  def change
    change_table :sections do |t|
      t.rename :resources_count, :resource_sections_count
    end
  end
end

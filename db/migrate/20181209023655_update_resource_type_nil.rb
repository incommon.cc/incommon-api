class UpdateResourceTypeNil < ActiveRecord::Migration[5.2]
  def change
    # Add default to Resource.type
    change_column :resources, :type, :string, limit: 16, default: 'Resource'
    # Set default for missing Resource type
    Resource.where(type: nil).update_all(type: 'Resource')
  end
end

class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.text :street
      t.string :pobox
      t.string :postal_code, limit: 16
      t.string :locality
      t.string :region
      t.string :country, limit: 2

      t.timestamps
    end
  end
end

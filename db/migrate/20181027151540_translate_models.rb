class TranslateModels < ActiveRecord::Migration[5.2]
  def up
    Mobility.locale = :en
    # Translate models
    [Taxonomy, Category, Section, Resource, Collection].each do |mod|
      mod.in_batches.each_record do |rec|
        [:name, :summary, :description].each do |col|
          rec.send(:"#{col}_backend").write(:en, rec[col])
        end
      end
    end

    # Remove columns
    [:resources, :taxonomies, :categories, :sections, :collections].each do |mod|
      [:name, :summary, :description].each do |col|
        remove_column mod, col
      end
    end           
  end

  def down
    # Add columns
    [:resources, :taxonomies, :categories, :sections, :collections].each do |mod|
      [:name, :summary].each do |col|
        add_column mod, col, :string
      end
      add_column mod, :description, :text
    end

    Mobility.locale = :en
    # 'Untranslate' models
    [Taxonomy, Category, Section, Resource, Collection].each do |mod|
      mod.in_batches.each_record do |rec|
        [:name, :summary, :description].each do |col|
          rec[col] = rec.send(:"#{col}_backend").read(:en)
        end
      end
    end
  end
end

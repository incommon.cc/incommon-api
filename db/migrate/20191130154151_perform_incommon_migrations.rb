class PerformIncommonMigrations < ActiveRecord::Migration[6.0]
  def change
    INCOMMON::API::Migrations.new.change
  end
end

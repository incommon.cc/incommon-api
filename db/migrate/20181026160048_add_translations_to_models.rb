class AddTranslationsToModels < ActiveRecord::Migration[5.2]
  def change
    add_column :categories,  :translations, :json, default: {}
    add_column :collections, :translations, :json, default: {}
    add_column :resources,   :translations, :json, default: {}
    add_column :sections,    :translations, :json, default: {}
    add_column :taxonomies,  :translations, :json, default: {}
  end
end

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id bigint NOT NULL,
    street text,
    pobox character varying,
    postal_code character varying(16),
    locality character varying,
    region character varying,
    country character varying(2),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    taxonomy_id bigint,
    color character varying(25),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rank integer,
    translations json DEFAULT '{}'::json,
    sections_count integer DEFAULT 0
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: collections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.collections (
    id bigint NOT NULL,
    agent_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    translations json DEFAULT '{}'::json
);


--
-- Name: collections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: collections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.collections_id_seq OWNED BY public.collections.id;


--
-- Name: emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.emails (
    id bigint NOT NULL,
    address character varying,
    sha256_hash character varying(64),
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.emails_id_seq OWNED BY public.emails.id;


--
-- Name: links; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.links (
    id bigint NOT NULL,
    uri text,
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.links_id_seq OWNED BY public.links.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.locations (
    id bigint NOT NULL,
    position_id bigint,
    locatable_type character varying,
    locatable_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.locations_id_seq OWNED BY public.locations.id;


--
-- Name: maps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.maps (
    id bigint NOT NULL,
    agent_id bigint,
    taxonomy_id bigint,
    position_id bigint,
    collection_id bigint,
    zoom smallint DEFAULT 13,
    uuid uuid,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: maps_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.maps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: maps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.maps_id_seq OWNED BY public.maps.id;


--
-- Name: phones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.phones (
    id bigint NOT NULL,
    number character varying(32),
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: phones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.phones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.phones_id_seq OWNED BY public.phones.id;


--
-- Name: positions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.positions (
    id bigint NOT NULL,
    geometry public.geometry,
    geo_type character varying,
    latitude numeric(9,7),
    longitude numeric(10,7),
    elevation numeric(7,2),
    radius integer,
    box json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: positions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.positions_id_seq OWNED BY public.positions.id;


--
-- Name: resource_addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_addresses (
    id bigint NOT NULL,
    address_id bigint,
    resource_type character varying,
    resource_id bigint,
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: resource_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_addresses_id_seq OWNED BY public.resource_addresses.id;


--
-- Name: resource_collections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_collections (
    id bigint NOT NULL,
    resource_id bigint,
    collection_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: resource_collections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_collections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_collections_id_seq OWNED BY public.resource_collections.id;


--
-- Name: resource_emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_emails (
    id bigint NOT NULL,
    email_id bigint,
    resource_type character varying,
    resource_id bigint,
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: resource_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_emails_id_seq OWNED BY public.resource_emails.id;


--
-- Name: resource_links; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_links (
    id bigint NOT NULL,
    link_id bigint,
    resource_type character varying,
    resource_id bigint,
    flags integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: resource_links_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_links_id_seq OWNED BY public.resource_links.id;


--
-- Name: resource_phones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_phones (
    id bigint NOT NULL,
    resource_type character varying,
    resource_id bigint,
    phone_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    flags integer DEFAULT 0 NOT NULL
);


--
-- Name: resource_phones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_phones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_phones_id_seq OWNED BY public.resource_phones.id;


--
-- Name: resource_sections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_sections (
    id bigint NOT NULL,
    resource_type character varying,
    resource_id bigint,
    section_id bigint
);


--
-- Name: resource_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resource_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resource_sections_id_seq OWNED BY public.resource_sections.id;


--
-- Name: resources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resources (
    id bigint NOT NULL,
    meta json,
    visible boolean,
    uuid uuid,
    type character varying(16) DEFAULT 'Resource'::character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    agent_id bigint,
    translations json DEFAULT '{}'::json
);


--
-- Name: resources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.resources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.resources_id_seq OWNED BY public.resources.id;


--
-- Name: resources_sections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resources_sections (
    resource_id bigint,
    section_id bigint
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    user_id bigint,
    agent_id bigint,
    roles integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sections (
    id bigint NOT NULL,
    category_id bigint,
    color character varying(25),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rank integer,
    translations json DEFAULT '{}'::json,
    resource_sections_count integer DEFAULT 0
);


--
-- Name: sections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sections_id_seq OWNED BY public.sections.id;


--
-- Name: taxonomies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.taxonomies (
    id bigint NOT NULL,
    uuid character varying(36),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    agent_id bigint,
    translations json DEFAULT '{}'::json,
    categories_count integer DEFAULT 0
);


--
-- Name: taxonomies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.taxonomies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taxonomies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.taxonomies_id_seq OWNED BY public.taxonomies.id;


--
-- Name: user_emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_emails (
    id bigint NOT NULL,
    email_id bigint,
    user_id bigint,
    flags integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: user_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_emails_id_seq OWNED BY public.user_emails.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username character varying,
    password_digest character varying,
    api_token uuid,
    uuid uuid,
    auth_token character varying,
    token character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sha256_hash character varying(64)
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: addresses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: collections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.collections ALTER COLUMN id SET DEFAULT nextval('public.collections_id_seq'::regclass);


--
-- Name: emails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.emails ALTER COLUMN id SET DEFAULT nextval('public.emails_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.links ALTER COLUMN id SET DEFAULT nextval('public.links_id_seq'::regclass);


--
-- Name: locations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.locations_id_seq'::regclass);


--
-- Name: maps id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.maps ALTER COLUMN id SET DEFAULT nextval('public.maps_id_seq'::regclass);


--
-- Name: phones id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phones ALTER COLUMN id SET DEFAULT nextval('public.phones_id_seq'::regclass);


--
-- Name: positions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.positions ALTER COLUMN id SET DEFAULT nextval('public.positions_id_seq'::regclass);


--
-- Name: resource_addresses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_addresses ALTER COLUMN id SET DEFAULT nextval('public.resource_addresses_id_seq'::regclass);


--
-- Name: resource_collections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_collections ALTER COLUMN id SET DEFAULT nextval('public.resource_collections_id_seq'::regclass);


--
-- Name: resource_emails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_emails ALTER COLUMN id SET DEFAULT nextval('public.resource_emails_id_seq'::regclass);


--
-- Name: resource_links id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_links ALTER COLUMN id SET DEFAULT nextval('public.resource_links_id_seq'::regclass);


--
-- Name: resource_phones id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_phones ALTER COLUMN id SET DEFAULT nextval('public.resource_phones_id_seq'::regclass);


--
-- Name: resource_sections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_sections ALTER COLUMN id SET DEFAULT nextval('public.resource_sections_id_seq'::regclass);


--
-- Name: resources id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resources ALTER COLUMN id SET DEFAULT nextval('public.resources_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: sections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sections ALTER COLUMN id SET DEFAULT nextval('public.sections_id_seq'::regclass);


--
-- Name: taxonomies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.taxonomies ALTER COLUMN id SET DEFAULT nextval('public.taxonomies_id_seq'::regclass);


--
-- Name: user_emails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_emails ALTER COLUMN id SET DEFAULT nextval('public.user_emails_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: collections collections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.collections
    ADD CONSTRAINT collections_pkey PRIMARY KEY (id);


--
-- Name: emails emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: maps maps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.maps
    ADD CONSTRAINT maps_pkey PRIMARY KEY (id);


--
-- Name: phones phones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phones
    ADD CONSTRAINT phones_pkey PRIMARY KEY (id);


--
-- Name: positions positions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);


--
-- Name: resource_addresses resource_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_addresses
    ADD CONSTRAINT resource_addresses_pkey PRIMARY KEY (id);


--
-- Name: resource_collections resource_collections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_collections
    ADD CONSTRAINT resource_collections_pkey PRIMARY KEY (id);


--
-- Name: resource_emails resource_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_emails
    ADD CONSTRAINT resource_emails_pkey PRIMARY KEY (id);


--
-- Name: resource_links resource_links_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_links
    ADD CONSTRAINT resource_links_pkey PRIMARY KEY (id);


--
-- Name: resource_phones resource_phones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_phones
    ADD CONSTRAINT resource_phones_pkey PRIMARY KEY (id);


--
-- Name: resource_sections resource_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_sections
    ADD CONSTRAINT resource_sections_pkey PRIMARY KEY (id);


--
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: sections sections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (id);


--
-- Name: taxonomies taxonomies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.taxonomies
    ADD CONSTRAINT taxonomies_pkey PRIMARY KEY (id);


--
-- Name: user_emails user_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_emails
    ADD CONSTRAINT user_emails_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_categories_on_taxonomy_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categories_on_taxonomy_id ON public.categories USING btree (taxonomy_id);


--
-- Name: index_collections_on_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_collections_on_agent_id ON public.collections USING btree (agent_id);


--
-- Name: index_locations_on_locatable_type_and_locatable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_locations_on_locatable_type_and_locatable_id ON public.locations USING btree (locatable_type, locatable_id);


--
-- Name: index_locations_on_position_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_locations_on_position_id ON public.locations USING btree (position_id);


--
-- Name: index_maps_on_collection_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_maps_on_collection_id ON public.maps USING btree (collection_id);


--
-- Name: index_maps_on_position_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_maps_on_position_id ON public.maps USING btree (position_id);


--
-- Name: index_maps_on_taxonomy_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_maps_on_taxonomy_id ON public.maps USING btree (taxonomy_id);


--
-- Name: index_maps_on_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_maps_on_uuid ON public.maps USING btree (uuid);


--
-- Name: index_phones_on_number; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_phones_on_number ON public.phones USING btree (number);


--
-- Name: index_positions_on_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_positions_on_geometry ON public.positions USING gist (geometry);


--
-- Name: index_resource_addresses_on_address_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_addresses_on_address_id ON public.resource_addresses USING btree (address_id);


--
-- Name: index_resource_addresses_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_addresses_on_resource_type_and_resource_id ON public.resource_addresses USING btree (resource_type, resource_id);


--
-- Name: index_resource_collections_on_collection_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_collections_on_collection_id ON public.resource_collections USING btree (collection_id);


--
-- Name: index_resource_collections_on_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_collections_on_resource_id ON public.resource_collections USING btree (resource_id);


--
-- Name: index_resource_emails_on_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_emails_on_email_id ON public.resource_emails USING btree (email_id);


--
-- Name: index_resource_emails_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_emails_on_resource_type_and_resource_id ON public.resource_emails USING btree (resource_type, resource_id);


--
-- Name: index_resource_links_on_link_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_links_on_link_id ON public.resource_links USING btree (link_id);


--
-- Name: index_resource_links_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_links_on_resource_type_and_resource_id ON public.resource_links USING btree (resource_type, resource_id);


--
-- Name: index_resource_phones_on_phone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_phones_on_phone_id ON public.resource_phones USING btree (phone_id);


--
-- Name: index_resource_phones_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_phones_on_resource_type_and_resource_id ON public.resource_phones USING btree (resource_type, resource_id);


--
-- Name: index_resource_sections_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_sections_on_resource_type_and_resource_id ON public.resource_sections USING btree (resource_type, resource_id);


--
-- Name: index_resource_sections_on_section_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resource_sections_on_section_id ON public.resource_sections USING btree (section_id);


--
-- Name: index_resources_on_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resources_on_agent_id ON public.resources USING btree (agent_id);


--
-- Name: index_resources_sections_on_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resources_sections_on_resource_id ON public.resources_sections USING btree (resource_id);


--
-- Name: index_resources_sections_on_section_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_resources_sections_on_section_id ON public.resources_sections USING btree (section_id);


--
-- Name: index_roles_on_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_agent_id ON public.roles USING btree (agent_id);


--
-- Name: index_roles_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_user_id ON public.roles USING btree (user_id);


--
-- Name: index_roles_on_user_id_and_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_roles_on_user_id_and_agent_id ON public.roles USING btree (user_id, agent_id);


--
-- Name: index_sections_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sections_on_category_id ON public.sections USING btree (category_id);


--
-- Name: index_taxonomies_on_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_taxonomies_on_agent_id ON public.taxonomies USING btree (agent_id);


--
-- Name: index_taxonomies_on_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_taxonomies_on_uuid ON public.taxonomies USING btree (uuid);


--
-- Name: index_user_emails_on_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_emails_on_email_id ON public.user_emails USING btree (email_id);


--
-- Name: index_user_emails_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_emails_on_user_id ON public.user_emails USING btree (user_id);


--
-- Name: resource_links fk_rails_0d7689a204; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_links
    ADD CONSTRAINT fk_rails_0d7689a204 FOREIGN KEY (link_id) REFERENCES public.links(id);


--
-- Name: resources_sections fk_rails_1c2fd8c3b7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resources_sections
    ADD CONSTRAINT fk_rails_1c2fd8c3b7 FOREIGN KEY (section_id) REFERENCES public.sections(id);


--
-- Name: resources_sections fk_rails_382fde77a0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resources_sections
    ADD CONSTRAINT fk_rails_382fde77a0 FOREIGN KEY (resource_id) REFERENCES public.resources(id);


--
-- Name: user_emails fk_rails_410ac92848; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_emails
    ADD CONSTRAINT fk_rails_410ac92848 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: resource_collections fk_rails_4e6a7a2987; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_collections
    ADD CONSTRAINT fk_rails_4e6a7a2987 FOREIGN KEY (resource_id) REFERENCES public.resources(id);


--
-- Name: maps fk_rails_56e7137436; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.maps
    ADD CONSTRAINT fk_rails_56e7137436 FOREIGN KEY (taxonomy_id) REFERENCES public.taxonomies(id);


--
-- Name: resource_collections fk_rails_5b93cd9e5d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_collections
    ADD CONSTRAINT fk_rails_5b93cd9e5d FOREIGN KEY (collection_id) REFERENCES public.collections(id);


--
-- Name: locations fk_rails_5c9f17a595; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT fk_rails_5c9f17a595 FOREIGN KEY (position_id) REFERENCES public.positions(id);


--
-- Name: maps fk_rails_785bd34e17; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.maps
    ADD CONSTRAINT fk_rails_785bd34e17 FOREIGN KEY (collection_id) REFERENCES public.collections(id);


--
-- Name: resource_sections fk_rails_873f0b3bae; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_sections
    ADD CONSTRAINT fk_rails_873f0b3bae FOREIGN KEY (section_id) REFERENCES public.sections(id);


--
-- Name: user_emails fk_rails_a98f61fc58; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_emails
    ADD CONSTRAINT fk_rails_a98f61fc58 FOREIGN KEY (email_id) REFERENCES public.emails(id);


--
-- Name: roles fk_rails_ab35d699f0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT fk_rails_ab35d699f0 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: sections fk_rails_ae2a3fc026; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sections
    ADD CONSTRAINT fk_rails_ae2a3fc026 FOREIGN KEY (category_id) REFERENCES public.categories(id);


--
-- Name: resource_emails fk_rails_b3e808b1d9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_emails
    ADD CONSTRAINT fk_rails_b3e808b1d9 FOREIGN KEY (email_id) REFERENCES public.emails(id);


--
-- Name: resource_addresses fk_rails_b92403a162; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_addresses
    ADD CONSTRAINT fk_rails_b92403a162 FOREIGN KEY (address_id) REFERENCES public.addresses(id);


--
-- Name: categories fk_rails_c048462d05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT fk_rails_c048462d05 FOREIGN KEY (taxonomy_id) REFERENCES public.taxonomies(id);


--
-- Name: maps fk_rails_c90b80a1a5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.maps
    ADD CONSTRAINT fk_rails_c90b80a1a5 FOREIGN KEY (position_id) REFERENCES public.positions(id);


--
-- Name: resource_phones fk_rails_d3df7057fb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_phones
    ADD CONSTRAINT fk_rails_d3df7057fb FOREIGN KEY (phone_id) REFERENCES public.phones(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20181009173241'),
('20181010180733'),
('20181010180927'),
('20181010181009'),
('20181015230252'),
('20181015232028'),
('20181015232056'),
('20181016011858'),
('20181016013125'),
('20181016013206'),
('20181016013231'),
('20181016015430'),
('20181016015915'),
('20181016021007'),
('20181016021104'),
('20181016023816'),
('20181016024858'),
('20181016053247'),
('20181016061230'),
('20181016062059'),
('20181016062351'),
('20181016075955'),
('20181018225548'),
('20181020164550'),
('20181021155943'),
('20181021174619'),
('20181023075729'),
('20181023114825'),
('20181023164809'),
('20181025143245'),
('20181026160048'),
('20181027151540'),
('20181028185905'),
('20181028222641'),
('20181106092911'),
('20181106094615'),
('20181106153013'),
('20181209023655'),
('20181209191924'),
('20181220123425');



# INCOMMON Models

## High-Level

### Map
### Resource
### Taxonomy

## Classifications

### Taxonomy

``` shell
rails g model Taxonomy name:string\{64\}:unique summary:string\{136\} \
    description:text uuid:uuid:unique agent:references categories_count:integer\{2\}
```

### Category

``` shell
rails g model Category name:string\{64\} summary:string\{136\} \
    description:text taxonomy:references sections_count:integer\{3\}
```

### Section

```shell
rails g model Section name:string\{80\} summary:string\{136\} \
    description:text category:references resources_count:integer
```

## Descriptors

### Event

### Location

### Tagging

## Generic

### Position

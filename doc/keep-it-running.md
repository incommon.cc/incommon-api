# INCOMMON Documentation

## Keep INCOMMON API Service Running

Related issue: https://framagit.org/incommon.cc/incommon-api/issues/20

This is to document how to keep the service running at boot.

We use a couple of scripts to run during startup, taking advantage of the `rc.local` facility. This documents how to enable it on GNU/Linux systems where it was broken by `systemd`.

### `bin/startup`

The Bash script in `bin/startup` is designed to run with our main setup in production, using `chruby` to select the current Ruby version and `sudo` to run the script as the `rails` user from `/srv/rails/incommon-api/`.

### `doc/run-incommon-api`

This lousy startup script is run by root upon system startup and can be used in a crontab as well to restart the API service in case it failed. It should be installed at `/usr/local/sbin/run-incommon-api`.

We run it from `/etc/rc.local`:

```bash
#!/bin/bash

# Run INCOMMON API
/usr/local/sbin/run-incommon-api

exit 0
```

### Activating rc.local for systemd

Since `systemd` broke all hell with classical *NIX sysadmin, the `rc.local` facility that used to just run is now disabled by default, and you must take a number of steps to make this simple service available again. Of course, if you're using `systemd` you should not use it and instead use the fantastic service files to not only boot your service, but monitor it and keep it alive. Well, that's the theory.

In practice:

```
chmod +x /etc/rc.local
cat <<CONFIG | tee /etc/systemd/system/rc-local.service
[Unit]
 Description=/etc/rc.local Compatibility
 ConditionPathExists=/etc/rc.local

[Service]
 Type=forking
 ExecStart=/etc/rc.local start
 TimeoutSec=0
 StandardOutput=tty
 RemainAfterExit=yes
 SysVStartPriority=99

[Install]
 WantedBy=multi-user.target
CONFIG
systemctl start rc-local.service # this should work
systemctl enable rc-local.service # then, this enables rc.local starting at next reboot
```

The `RemainAfterExit=yes` sounds dubious to me: it should rather say no, since once the script has run, all execution should have terminated or forked.

## Keeping it live!

For now we did not setup a facility -- besides cron -- to maintain the API service live: it's a good indication when it crashes, that something went wrong. Please reopen the issue or reference it in a merge request if you want to make things right.

If you want to add the `run-incommon-api` script to cron, open your editor for root crontab, and write, to check that it runs every minute:

```crontab
# Keep INCOMMON API live
* * * * * /usr/local/sbin/run-incommon-api
```



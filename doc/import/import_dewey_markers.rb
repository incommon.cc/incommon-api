# frozen_string_literal: true

require_relative '../../config/application'
require_relative '../../config/environment'

require 'json'

## Agent

agent = Agent.i18n.find_by(name: 'Dewey Maps')
unless agent
  raise ArgumentError,
        'Dewey ASBL Agent does not exist!' \
        ' Please run ./import_dewey_maps_categories.rb first.'
end

## Classification

taxonomy = agent.taxonomies.i18n.find_by(name: 'Dewey Maps')
unless taxonomy
  raise ArgumentError,
        'Dewey Maps Taxonomy does not exist!' \
        ' Please run ./import_dewey_maps_categories.rb first.'
end

collection = agent.collections.create(
  name: 'Dewey Maps Collection',
  summary: 'Collection visible at https://maps.dewey.be'
)

Rails.logger.info 'Importing points...'

entries = JSON.parse File.read(Rails.root.join('doc', 'import', 'dewey-maps-markers.json'))
subcat  = JSON.parse File.read(Rails.root.join('doc', 'import', 'dewey-maps-sections.json'))

# rubocop:disable Metrics/BlockLength
entries.each do |ent|
  print '.'

  pos = Position.find_or_create_by(geometry: ent['fields']['position'], geo_type: 'Point')

  # Extra properties
  xtr = {
    pk: ent['pk'],
    manager_type: ent['fields']['manager_type'],
    source: 'Dewey Maps',
  }

  link = nil
  url  = ent['fields']['web'].strip
  link = Link.with_uri!(url) if url.present?

  phone = nil
  phnum = ent['fields']['phone'].gsub(/[^\d]/, '').strip
  begin
    unless phnum.nil?
      phone = Phone.find_or_create_by(
        number: PhonyRails.normalize_number(phnum, add_plus: true, country_code: 'BE')
      )
    end
  rescue StandardError => e
    Rails.logger.warn "Phone creation failed for #{phnum}: #{e.message}"
    xtr[:phone_number] = phnum if phnum.present?
  end
  xtr[:phone_number] = phnum unless phone.valid? || xtr.key?(:phone_number)

  # address
  xtr[:address] = ent['fields']['adress'] # sic
  Regexp.new(/\A(.+)\s(\d+)\s([\w-]+)\z/).match(xtr[:address])
  street = Regexp.last_match(1)
  postal_code = Regexp.last_match(2)
  locality = Regexp.last_match(3)
  if postal_code && locality
    adr = Address.create(
      street: street,
      postal_code: postal_code,
      locality: locality,
      country: 'BE'
    )
    xtr.delete(:address) if adr.valid?
  end

  email = nil
  if ent['fields']['email'].present?
    email_address = ent['fields']['email'].delete(' ').strip
    email = Email.with_address!(email_address) unless email_address.nil?
  end

  res = Resource.create(name: ent['fields']['name'],
                        description: ent['fields']['comment'],
                        visible: (ent['fields']['public'] == true),
                        created_at: ent['fields']['created'],
                        updated_at: ent['fields']['last_modified'],
                        agent_id: agent.id,
                        type: 'Resource',
                        meta: xtr)

  if res&.valid?
    # Add resource properties
    res.resource_addresses.create(address: adr) if adr&.valid?
    res.resource_emails.create(email: email)    if email&.valid?
    res.resource_links.create(link: link)       if link&.valid?
    res.resource_phones.create(phone: phone)    if phone&.valid?
    res.locations.create(position: pos)
    res.save
    # Register to Agent's collection
    collection.resource_collections.create(resource: res)
    msg = "[**] Entry saved #{ent['pk']} -> #{res.id} : #{res.name}"
    Rails.logger.info msg

    ent['fields']['subcategories'].each do |sec_id|
      sec = Section.find_by(id: subcat[sec_id.to_s])
      if sec
        res.sections << sec
        res.save
        sec.save
      else
        msg = "[!!] Missing section for sec_id '#{sec_id}'"
        res.meta[:missing_subcat] = sec_id
        res.save
        Rails.logger.warn msg
      end
    end
  else
    Rails.logger.warn "[!!] Entry failed to save:\n#{res.errors.inspect}"
    sleep 3
  end
end
# rubocop:enable Metrics/BlockLength

collection.save

# Create map

x = 4.35308
y = 50.83906
z = 12
map_center = Position.find_or_create_by(
  geometry: "POINT(#{x} #{y})",
  latitude: y,
  longitude: x,
  geo_type: 'Point'
)

Map.create(
  agent: agent,
  collection: collection,
  position: map_center,
  taxonomy: taxonomy,
  zoom: z
)

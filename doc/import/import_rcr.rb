# frozen_string_literal: true

require_relative '../../config/application'
require_relative '../../config/environment'

require 'json'

agent = Agent.find_by(name: 'Réseau de Consommateurs Responsables')

raise ArgumentError, "RCR Agent does not exist!\n#{agent&.errors&.inspect}" unless agent

entries = JSON.parse File.read(Rails.root.join('doc', 'import', 'rcr-initiatives.json'))
rcr     = agent.taxonomies.includes(:categories, :sections)
               .find_by(name: 'RCR').categories.map(&:sections).flatten

# Data from RCR are very groovy. You can find letters in phone numbers and other
# inconsistencies. Therefore we try our best to import everything in one go and
# keep all original data in the Resource.properties arbitrary field so that
# humans can manually fix these inconsistencies.

def find_section_for(ent, sections)
  sections_by_initiative = {
    autre: 'Autres initiatives',
    donnerie: ent['type_donnerie'],
    gac: 'Groupement d\'Achat Coopératif (GAC)',
    potager: {
      unspecified: nil,
      pedagogique: 'Potager pédagogique',
      scolaire: 'Potager scolaire',
      cpas: 'Centre Public d\'Aide Sociale (CPAS)',
      eft: 'EFT',
      citoyen: 'Potager citoyen',
    },
    repaircafe: 'Repair Café',
    res: 'RES',
    sel: 'Système d\'Échange Local (SEL)',
  }

  sections.find do |i|
    i.name == if ent['type_initiative'] == 'potager'
                typot = ent['type_potager'].to_s.strip || 'unspecified'
                sections_by_initiative[:potager][typot]
              else
                sections_by_initiative[ent['type_initiative']]
              end
  end
end

def create_resource(entry, agent)
  meta = {
    source: 'RCR',
    source_id: entry['id_initiative_rcr'],
    source_original_data: entry,
  }
  # Skip phones for now: the field is randomly inconsistent
  meta[:address] = entry['addresse_initiative'] if entry['addresse_initiative'].present?

  attrs = {
    name: entry['nom_initiative'],
    agent_id: agent.id,
    meta: meta,
  }

  res = Resource.create(attrs)

  # Some resources are not geolocalized!
  add_position_to(res, entry['geolocation_initiative'])
  # Some resources don't have a website
  add_websites_to(res, entry['website_initiative'])
  # Some resources don't have an email
  add_emails_to(res, entry['courriel_initiative'])

  res.save && res
end

def add_position_to(res, position)
  return if position.blank?

  pos = Position.find_or_create_by(geometry: position)
  res.locations.build(position: pos)
end

def add_websites_to(res, websites)
  return if websites.blank?

  websites.each do |url|
    url.to_s.strip!
    next if url.nil?

    # Ensure we have a protocol
    url = 'http://' + url unless url =~ %r{^https?://}
    res.resource_links.build(link: Link.find_or_create_by(uri: url))
  end
end

def add_emails_to(res, emails)
  return if emails.blank?

  emails.each do |email_address|
    email_address.to_s.strip!
    next if email_address.blank?

    email = Email.with_address!(email_address)
    res.resource_emails.build(email: email) if email.valid?
  end
end

# Create a resource for each commune
def add_communes_to(res, communes)
  communes&.each do |comm|
    c = res.agent.places.find_or_create_by(name: comm['nom_commune'])
    c.meta = {
      source: 'RCR',
      source_original_data: comm,
      place_type: 'city',
    }
    p = Position.find_or_create_by(geometry: comm['point_geolocalise'])
    c.locations.build(position: p) if c.id_before_last_save.nil?
    c.save!
    add_collections_for(c, res)
  rescue StandardError => e
    Rails.logger.error "Exception: #{e.class} / #{e.message}"
    p c
  end
end

def add_collections_for(city, resource)
  # Create a collection for each city
  coll = res.agent.collections.find_or_create_by(name: city.name)
  coll.meta = {
    source: 'RCR',
    collection_type: "Resources in commune '#{city.name}'",
  }
  coll.resource_collections.build(resource: city)
  coll.resource_collections.build(resource: resource)
  coll.save
rescue StandardError => e
  Rails.logger.error "Exception: #{e.class} / #{e.message}"
  Rails.logger.error city.inspect
end

def add_contacts_to(res, contacts)
  contacts&.each do |contact|
    email_address = contact['courriel'].to_s.strip
    if email_address.blank?
      res.meta[:missing_contact_email] ||= []
      res.meta[:missing_contact_email] << contact
      next
    end
    a = AgentCreator.create(
      name: "#{contact['prenom']} #{contact['nom']}",
      email: email_address
    )
    a.visible = contact['type_d_identite'] != 'Personne physique'
    a.meta = {
      source: 'RCR',
      source_original_data: contact,
    }
    a.save
    # Pass the resource to the contact agent if it's a collective
    res.update_attribute(:agent_id, a.id) if a.visible?
  end
end

## Loop through Entries

entries['initiatives'].each do |ent|
  print '.'

  initiative = ent['initiative']

  res = create_resource(initiative, agent)
  raise StandardError, "Resource could not be created! #{res.inspect}" if res.blank?

  if res.valid?
    sec = find_section_for(initiative, rcr)
    if sec.nil?
      unless initiative.key?('type_potager')
        res.meta['missing_section'] = initiative['type_initiative']
      end
    else
      res.sections << sec
    end

    add_contacts_to(res, initiative['contact'])
    add_communes_to(res, initiative['communes'])

    res.save
  else
    warn '[!!] Entry failed to save:'
    p res.errors
    sleep 3
  end
end

# Find Duplicate Resources

Sometimes import brings up exact copies of a single resources, cluttering the
database with duplicates. Here's how to detect them and handle them.

``` ruby
rhashes = {}
Resource.all.each { |r| rhashes[r.id] = Digest::SHA256.hexdigest(r.properties.to_s) }
dups = {}
rhashes.each { |k, v| dups[v] ||= []; dups[v] << k }
dups.delete_if { |k, v| v.length == 1 }
```

So, we went through all resources and hashed their `properties`, to detect which
ones are exact copies. Then we grouped duplicate resources by their hash, and
removed unique resources. Now we can loop through each duplicates to see what
needs to be done (since each resource may belong to different agents, appear in
different categories, and be attached to various collections...)

**Beware of `nil`**: if `properties` is `nil`, then it hashes the same, and it's
a false positive: the given resources are not duplicates, they simply lack
metadata.

In the original phase of import from Dewey Maps, I removed 665 duplicates, which
were indeed versions of the same resource. In this case I knew the latest
version was the right one:

``` ruby
dups.each { |k, v| v.pop; v.size.times { Resource.destroy(v.shift) } }
```

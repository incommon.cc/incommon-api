# frozen_string_literal: true

require 'sso/from_discourse'

# Perform Single Sign-On using Discourse
module SSO
  require 'securerandom'
  require_relative '../../config/initializers/sso_config'
end

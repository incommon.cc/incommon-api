# frozen_string_literal: true

module INCOMMON
  module API
    require_dependency 'incommon/api/errors'
    require_dependency 'incommon/api/controller'
    require_dependency 'incommon/api/jsonapi_serializable_helper'

    attr_accessor :key

    # The current API key
    # See INCOMMON::Functions.api_key
    def self.key
      @key ||= INCOMMON.api_key
    end

    # Specify the current API key
    def self.key=(api_key)
      @key = api_key
    end

    # Return the application version
    def self.version
      INCOMMON::Version
    end

    # Return only the MAJOR version for API calls
    def self.api_version
      "v#{version.split('.').first}"
    end
  end
end

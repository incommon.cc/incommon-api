# frozen_string_literal: true

require 'digest/sha2'

module INCOMMON
  module Utils
    # Ensure Rails runs in production mode, if Rails is running at all.
    def rails_in_production? # :nodoc:
      if defined?(::Rails)
        ::Rails.env.production?
      else
        ENV.fetch('RAILS_ENV', 'no_rails') == :production
      end
    end

    # Time-constant string comparison
    #
    # Other implementations return straight away when a and b have a different
    # bytesize, which seems to defeat the purpose to time-constant
    # comparison. Instead we go all the way even if we already know the
    # comparison has failed.
    #
    # rubocop:disable Naming/UncommunicativeMethodParamName
    # No, rubocop, z |= x ^ y is not equivalent to z | x ^ y.
    # rubocop:disable Lint/UselessAssignment
    def tc_string_compare(a, b)
      Digest::SHA256.hexdigest(a.to_s).bytes
                    .zip(Digest::SHA256.hexdigest(b.to_s).bytes)
                    .reduce(0) do |z, x|
        z |= x[0] ^ x[1]
      end.zero? && !any_blank?(a, b) && (a.bytesize == b.bytesize)
    end
    # rubocop:enable Lint/UselessAssignment
    # rubocop:enable Naming/UncommunicativeMethodParamName

    # Return true if any of the arguments if nil, empty or not a string
    def any_blank?(*args)
      args.each { |a| return true unless (a.respond_to?(:empty?) && !a.empty?) || !a.nil? }
      false
    end
  end
  include INCOMMON::Utils
end

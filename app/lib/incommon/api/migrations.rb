# frozen_string_literal: true

module INCOMMON
  module API
    class Migrations < ActiveRecord::Migration[5.2]
      # rubocop:disable Metrics/MethodLength
      # rubocop:disable Style/TrailingCommaInHashLiteral
      # ^^ This is because heredoc is not properly supported within a
      # hash and Rubocop wants to turn <<-DESCRIPTION into <<-DESCRIPTION,
      def change
        if User.find_by(id: User::ANONYMOUS_USER_ID).nil?
          # Create INCOMMON::AnonymousUser
          User.create(
            id: User::ANONYMOUS_USER_ID,
            username: 'anon@incommon.cc',
            api_token: '23d8009f-f2ec-44b6-bcbe-58ad1d3af10b',
            uuid: '6c8be57b-5730-428d-bdf2-ff9bcb37d7fd'
          )
        end
        if Agent.find_by(id: Agent::INCOMMON_AGENT_ID).nil?
          # Create INCOMMON::IncommonAgent
          Agent.create(
            id: Agent::INCOMMON_AGENT_ID,
            visible: true,
            translations: {
              en: {
                name: 'IN COMMON Agent',
                summary: 'Default Agent for the IN COMMON Collective.',
                description: <<-DESCRIPTION
                ## IN COMMON Agent

                When no Agent is available to manage Resources, this Agent is used.

                ### Call Sequence

                ``` ruby
                agent = Agent.incommon_agent
                agent.name # => 'IN COMMON Agent'
                ```

                ### Functionality

                `IncommonAgent` works like any other Agent, except:
                - it is always **not** authenticated: `agent.authenticated? # => false`
                - it is always an editor: `agent.editor? # => true`
                  This allows to create new resources anywhere.
                - it has no other role, e.g.: `agent.maintainer? # => false`
                  This prevents from editing or observing Resources from other Agents,
                  and prevents from changing user Roles.

                If you encounter an issue with `INCOMMON::IncommonAgent`, please report
                it to <https://framagit.org/incommon/incommon-api>.
                DESCRIPTION
              },
              fr: {
                name: 'Agent IN COMMON',
                summary: "L'Agent par défaut pour le collectif IN COMMON.",
                description: <<-DESCRIPTION
                ## Agent IN COMMON

                Lorsqu'aucun Agent n'est disponible pour maintenir des Ressource,
                on utilise cet Agent.

                ### Utilisation

                ``` ruby
                Mobility.locale = :fr
                agent = Agent.incommon_agent
                agent.name # => 'Agent IN COMMON'
                ```

                ### Fonctionalité

                `IncommonAgent` fonctionne comme n'importe quel autre Agent, avec les
                exceptions suivantes :
                - il n'est **jamais** identifié : `agent.authenticated? # => false`
                - il est toujours un éditeur : `agent.editor? # => true`
                  Cela permet de créer de nouvelles Ressources n'importe où.
                - il n'a aucun autre rôle, par exemple : `agent.maintainer? # => false`
                  Cela empêche la modification ou l'observation de Ressources appartenant
                  à d'autres Agents, et cela prévient la modification des Rôles
                  d'utilisateurs.

                Si vous découvrez un problème avec `INCOMMON::IncommonAgent`, merci de le
                rapporter vers <https://framagit.org/incommon/incommon-api>.
                DESCRIPTION
              },
            },
            uuid: '7218b579-a1b4-4318-9fa8-e8dbff2a61c0'
          )
        end
        if Agent.incommon_agent.roles.empty?
          Role.create(
            agent_id: Agent::INCOMMON_AGENT_ID,
            user_id: User::ANONYMOUS_USER_ID,
            roles: Role.bitfields[:roles][:editor]
          )
        end
        # rubocop:enable Style/TrailingCommaInHashLiteral
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end

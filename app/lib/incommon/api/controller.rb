# frozen_string_literal: true

module INCOMMON
  module API
    module Controller
      extend ActiveSupport::Concern

      included do
        before_action :current_user
        before_action :set_locale

        rescue_from NotImplementedError do |exception|
          render_jsonapi_error_not_implemented(exception.message)
        end
        rescue_from INCOMMON::API::Errors::UnknownRelationshipError do |exception|
          render_jsonapi_error_unknown_relationship(exception.message)
        end
        rescue_from INCOMMON::API::Errors::UnauthorizedError do |exception|
          render_jsonapi_error_unauthorized(exception.message)
        end
      end

      # Set current Agent for authorization context.
      # We need to call it current_user for Pundit to consume it.
      def current_user
        @current_user ||= INCOMMON::CurrentAgent.new(active_user, params[:agent_id])
      end

      # Set current User for authentication context with current Agent.
      def active_user
        @active_user ||= INCOMMON::CurrentUser.new(request)
      end

      private

      # Validate params[:include] against model_types available to this controller.
      def include_from_params
        (params[:include] || '').split(',').reduce([]) do |memo, value|
          if model_types.include?(value)
            memo << value
          else
            # Here we use api.help instead of api.error for the exception
            # message to keep the context.
            # @see render_jsonapi_error_unknown_relationship
            raise INCOMMON::API::Errors::UnknownRelationshipError,
                  I18n.t('api.help.unknown_relationship',
                         relationship: value, known: model_types.to_sentence)
          end
        end
      end

      # Return an array of available model types for this JSONAPI resource.
      # This must be implemented in each API controller.
      def model_types
        raise NotImplementedError, I18n.t('api.errors.model_types_not_implemented')
      end

      # Render a generic JSONAPI error
      def render_jsonapi_error(options)
        render(jsonapi_errors: {
                 title: options[:error_message],
                 detail: options[:detail],
                 links: options[:links],
               }, status: options[:status])
      end

      # Render a generic JSONAPI error for HTTP Error 400.
      def render_jsonapi_bad_request(error_message, detail, links)
        options = {
          error_message: error_message,
          detail: detail,
          links: links,
          status: :bad_request,
        }
        render_jsonapi_error(options)
      end

      # Render a generic JSONAPI error for HTTP Error 401.
      def render_jsonapi_error_unauthorized(error_detail)
        options = {
          error_message: I18n.t('api.errors.unauthorized'),
          detail: error_detail,
          links: { about: 'https://talk.incommon.cc/api_error_401' },
          status: :unauthorized,
        }
        render_jsonapi_error(options)
      end

      # Render a generic JSONAPI error for HTTP Error 501.
      def render_jsonapi_error_not_implemented(error_detail)
        options = {
          error_message: I18n.t('api.errors.not_implemented'),
          detail: error_detail,
          links: { about: 'https://talk.incommon.cc/api_error_501' },
          status: :not_implemented,
        }
        render_jsonapi_error(options)
      end

      # Render a useful JSONAPI error message when params[:include] mentions an
      # unknown relationship for the requested resource. The passed message is
      # the detail message, not the error message!
      def render_jsonapi_error_unknown_relationship(error_detail)
        options = {
          error_message: I18n.t('api.errors.unknown_relationship'),
          detail: error_detail,
          links: { about: 'https://jsonapi.org/format/#fetching-includes' },
          status: :bad_request,
        }
        render_jsonapi_error(options)
      end

      # Set locale from params[:locale], fallback to default locale.
      def set_locale
        locale = I18n.default_locale
        if params.key?(:locale) && I18n.available_locales.include?(params[:locale].to_sym)
          locale = params[:locale].to_sym
        end
        I18n.locale = locale
      end
    end
  end
end

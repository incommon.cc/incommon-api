# frozen_string_literal: true

module INCOMMON
  module API
    # == API Routing Constraints
    #
    # This is used in +IncommonApi+ routes to switch API versions.
    #
    # By default the current stable API version is used (currently v0).
    #
    # API consumers can pass the version along with the +Accept+ header:
    #
    #  curl -H 'Accept: application/vnd.api+json;version=0' ...
    #
    # This is not a standard way (people tend to prefer partitioning their URLs
    # to explicitly mention the version, but it makes things simpler for
    # developers since routes never change across the application.
    #
    # Then it allows clients to use a version-specific route simply by changing
    # a header, even on a per-request basis. Note that the JSONAPI specific
    # mentions it's forbidden to use media parameters in the header: but this is
    # a custom header, not a media header, and it might become standardized at
    # some point.
    #
    class Constraints
      # The standard JSONAPI MIME type
      API_MIME_TYPE = 'application/vnd.api+json'

      # It accepts a single options hash with optional arguments: the version
      # (only the SemVer MAJOR version, e.g., 0), and a boolean to indicate
      # whether it's the default version or not.
      #
      # It might be tempting to use options[:default] = true for all routes,
      # but it will break things. Don't do it. It only makes sense to default to
      # the current stable version, otherwise clients will get confused,
      # especially if there are breaking changes in future versions.
      #
      def initialize(options = {})
        @version = options[:version] || 0
        @default = options[:default] || false
      end

      # Pass the current +request+ and check the +Accept+ header for JSONAPI
      # mime type and optionally the IN COMMON API version.
      def matches?(req)
        @default || (
          accept = req.headers['Accept']
          v = Regexp.last_match(1) if accept =~ %r{#{API_MIME_TYPE}(?:;\s*version=(\d+))}
          Rails.logger.info "Request matches version '#{v.to_i}'"
          @version.to_s == v.to_i.to_s
        )
      end
    end
  end
end

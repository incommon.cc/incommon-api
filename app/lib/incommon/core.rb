# frozen_string_literal: true

module INCOMMON
  module Core
    # Ensure Rails runs in production mode, if Rails is running at all.
    def rails_in_production? # :nodoc:
      if defined?(::Rails)
        ::Rails.env.production?
      else
        ENV.fetch('RAILS_ENV', 'no_rails') == :production
      end
    end

    # Time-constant string comparison
    #
    # Other implementations return straight away when a and b have a different
    # bytesize, which seems to defeat the purpose of time-constant
    # comparison. Instead we go all the way even if we already know the
    # comparison has failed.
    #
    # rubocop:disable Naming/UncommunicativeMethodParamName
    # No, rubocop, z |= x ^ y is not equivalent to z | x ^ y.
    # rubocop:disable Lint/UselessAssignment
    def tc_string_compare(a, b)
      Digest::SHA256.hexdigest(a.to_s).bytes
                    .zip(Digest::SHA256.hexdigest(b.to_s).bytes)
                    .reduce(0) do |z, x|
        z |= x[0] ^ x[1]
      end.zero? && !any_blank?(a, b) && (a.bytesize == b.bytesize)
    end
    # rubocop:enable Lint/UselessAssignment
    # rubocop:enable Naming/UncommunicativeMethodParamName

    # Return true if any of the arguments if nil, empty or not a string
    def any_blank?(*args)
      args.each { |a| return true unless (a.respond_to?(:empty?) && !a.empty?) || !a.nil? }
      false
    end
  end

  class << self
    include Core
  end

  # This is the default public read-only API key
  #
  # You can use it in place of your own for *read-only* access to the IN COMMON
  # API:
  #
  #  curl -H 'Accept: application/vnd.api+json' \
  #       -X GET 'https://api.incommon.cc/taxonomies?api_key=23d8009f-f2ec-44b6-bcbe-58ad1d3af10b'
  #
  API_KEY_PUBLIC = '23d8009f-f2ec-44b6-bcbe-58ad1d3af10b'

  # During +development+, use +api.example.net+.
  #
  # In +/etc/hosts+, add:
  #   127.0.0.1 api.example.net
  #
  # See doc/development_setup.md
  #
  API_ROOT_URL = if rails_in_production?
                   'https://api.incommon.cc'
                 else
                   'https://api.example.net'
                 end
end

# frozen_string_literal: true

module EmailHelper
  module_function

  def hash_email(address)
    Digest::SHA256.hexdigest(address.to_s)
  end
end

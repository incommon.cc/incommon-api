# frozen_string_literal: true

module UrlHelper
  def policy_exception_doc_url(policy_name)
    "https://doc.incommon.cc/exceptions/#{policy_name}"
  end
end

# frozen_string_literal: true

class SerializableCollection < JSONAPI::Serializable::Resource
  type 'collections'

  # attribute :id
  attribute :name
  attribute :summary
  attribute :description
  # attribute :translations

  attribute :created_at
  attribute :updated_at

  belongs_to :agent do
    link :self do
      @url_helpers.api_agent_url(@object.agent)
    end
  end

  has_many :maps

  has_many :resource_collections

  has_many :resources, through: :resource_collections do
    data do
      @object.resources
    end
    meta do
      {
        resource_count: @object.resources.count,
      }
    end
  end

  link :self do
    @url_helpers.api_collection_url(@object)
  end

  meta do
    meta_current
  end
end

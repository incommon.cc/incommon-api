# frozen_string_literal: true

# User this instead of JSONAPI::Serializable::Resource as the parent class.
class SerializableIncommonResource < JSONAPI::Serializable::Resource
  # Fix at https://github.com/jsonapi-rb/jsonapi-serializable/issues/80
  extend JSONAPI::Serializable::Resource::ConditionalFields
end

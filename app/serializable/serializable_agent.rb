# frozen_string_literal: true

# SerializableAgent is a special case of SerializableResource since an Agent's
# Agent is itself. So we don't inherit from SerializableResource to avoid
# complicating things. Beware though of changes in Resource model that might
# affect Agent as well.
class SerializableAgent < SerializableIncommonResource
  type 'agents'

  id { @object.uuid }

  # attribute :id

  # Mobility translations
  attribute :name
  attribute :summary
  attribute :description

  attribute :type
  attribute :uuid
  attribute :visible

  attribute :created_at
  attribute :updated_at

  has_many :collections, unless: -> { restricted? }
  has_many :users,       if:     -> { leader? }

  has_many :roles do
    meta do
      {
        editor_count: @object.editors.count,
        maintainer_count: @object.maintainers.count,
        leader_count: @object.leaders.count,
        observer_count: @object.observers.count,
      }
    end
  end
  with_options if: -> { leader? } do
    has_many :editors
    has_many :maintainers
    has_many :observers
    has_many :leaders
  end

  with_options if: -> { maintainer? } do
    has_many :resource_addresses
    has_many :resource_emails
    has_many :resource_links
    has_many :origins
    has_many :resource_phones
    has_many :taxonomies
  end

  link :self do
    @url_helpers.api_agent_url(@object)
  end

  meta do
    meta_current
  end
end

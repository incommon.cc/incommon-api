# frozen_string_literal: true

class SerializableLink < JSONAPI::Serializable::Resource
  type 'links'

  attribute :uri

  # Flags
  %w(secure not_found cert_error server_error).each do |flag|
    attribute :"#{flag}" do
      @object.send(:"#{flag}?")
    end
  end

  attribute :created_at
  attribute :updated_at

  has_many :agents
  has_many :resource_links

  meta do
    meta_current
  end
end

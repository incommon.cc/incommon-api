# frozen_string_literal: true

class SerializableResourceAddress < JSONAPI::Serializable::Resource
  type 'resource_addresses'

  attribute :resource_type

  # Flags
  attribute :flags

  attribute :created_at
  attribute :updated_at

  has_one :main_address
  has_one :address
  has_one :resource
  has_one :agent

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class SerializableUser < SerializableIncommonResource
  type 'users'

  id { @object.uuid }

  attribute :username do
    leader_or_me? ? @object.username : @object.sha256_hash
  end
  attribute :password do
    redacted
  end
  attribute :api_token do
    me? ? @object.api_token : redacted
  end
  attribute :uuid
  attribute :auth_token do
    me? ? @object.auth_token : redacted
  end
  attribute :token do
    redacted
  end
  attribute :created_at
  attribute :updated_at
  attribute :sha256_hash, if: -> { leader_or_me? }

  has_many :agents, if: -> { leader_or_me? } do
    data do
      if me?
        @object.agents
      elsif leader?
        @object.agents.where(id: @current_user.id)
      else
        redacted
      end
    end
    meta count: @object.agents.count
  end

  has_many :roles, if: -> { leader_or_me? } do
    if me?
      @object.roles
    elsif leader?
      @current_user.roles
    else
      redacted
    end
  end

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class SerializableRole < JSONAPI::Serializable::Resource
  type 'roles'

  # attribute :roles
  attribute :editor do
    @object.editor?
  end
  attribute :leader do
    @object.leader?
  end
  attribute :maintainer do
    @object.maintainer?
  end
  attribute :observer do
    @object.observer?
  end

  attribute :agent_uuid do
    @object.agent.uuid
  end
  attribute :user_uuid do
    @object.user.uuid
  end

  attribute :created_at
  attribute :updated_at

  belongs_to :agent do
    link :self do
      @url_helpers.api_agent_url(@object.agent.uuid)
    end
  end

  belongs_to :user do
    link :self do
      @url_helpers.api_user_url(@object.user.uuid)
    end
  end

  link :self do
    @url_helpers.api_role_url(@object)
  end

  meta do
    meta_current
  end
end

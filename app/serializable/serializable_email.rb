# frozen_string_literal: true

class SerializableEmail < JSONAPI::Serializable::Resource
  type 'emails'

  attribute :address do
    restricted? ? @object.sha256_hash : @object.address
  end
  # attribute :flags
  attribute :bouncing do
    @object.bouncing?
  end
  attribute :failing do
    @object.failing?
  end

  attribute :created_at
  attribute :updated_at

  has_many :resource_emails do
    meta do
      {
        resources_count: @object.resources.count,
        entities_count: @object.entities.count,
        places_count: @object.places.count,
        services_count: @object.services.count,
        things_count: @object.things.count,
      }
    end
  end

  meta do
    meta_current
  end
end

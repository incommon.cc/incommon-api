# frozen_string_literal: true

class SerializableAddress < JSONAPI::Serializable::Resource
  type 'addresses'

  attribute :street
  attribute :pobox
  attribute :postal_code
  attribute :locality
  attribute :region
  attribute :country

  attribute :created_at
  attribute :updated_at

  has_many :resource_addresses do
    meta do
      {
        resources_count: @object.resources.count,
        entities_count: @object.entities.count,
        places_count: @object.places.count,
        services_count: @object.services.count,
        things_count: @object.things.count,
      }
    end
  end

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class SerializableResource < SerializableIncommonResource
  type { @object.type.pluralize.underscore }

  id   { @object.uuid }

  # Mobility translations
  attribute :name
  attribute :summary
  attribute :description

  # Present primary properties as attributes
  %w(address email link phone).each do |prop|
    attribute "main_#{prop}".to_sym do
      @object.send("main_#{prop}")
    end
  end

  attribute :type
  attribute :uuid
  attribute :visible

  attribute :created_at
  attribute :updated_at

  belongs_to :agent

  # Descriptors
  has_many :locations

  # Relations
  with_options if: -> { @object.type == 'Agent' && maintainer? } do
    has_many :resource_addresses
    has_many :resource_emails
    has_many :resource_links
    has_many :resource_phones
  end

  # Properties
  has_many :addresses
  has_many :emails
  has_many :links
  has_many :phones

  # Resource collections
  has_many :collections, unless: -> { @object.type == 'Agent' && restricted? }
  # Resource sections (taxonomy)
  has_many :sections,    if:     -> { @object.type == 'Agent' && leader? }

  link :self do
    @url_helpers.api_resource_url(@object)
  end

  meta do
    meta_current
  end
end

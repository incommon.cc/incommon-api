# frozen_string_literal: true

class SerializablePosition < JSONAPI::Serializable::Resource
  type 'positions'

  attribute :box
  attribute :elevation
  attribute :geo_type
  attribute :geometry
  attribute :latitude
  attribute :longitude
  attribute :radius

  attribute :created_at
  attribute :updated_at

  has_many :locations

  link :self do
    @url_helpers.api_position_url(@object)
  end

  meta do
    meta_current
  end
end

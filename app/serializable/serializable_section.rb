# frozen_string_literal: true

class SerializableSection < JSONAPI::Serializable::Resource
  type 'sections'

  attribute :name
  attribute :summary
  attribute :description

  attribute :color
  attribute :created_at
  attribute :updated_at

  belongs_to :category
  has_many   :resources do
    meta count: @object.resources_count
  end

  link :self do
    @url_helpers.api_section_url(@object)
  end

  meta do
    meta_current
  end
end

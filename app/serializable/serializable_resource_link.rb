# frozen_string_literal: true

class SerializableResourceLink < JSONAPI::Serializable::Resource
  type 'resource_links'

  # Flags
  attribute :homepage do
    @object.homepage?
  end
  attribute :icon do
    @object.icon?
  end
  attribute :origin do
    @object.origin?
  end

  attribute :resource_type

  attribute :created_at
  attribute :updated_at

  belongs_to :agent
  belongs_to :link
  belongs_to :resource

  has_many :origins

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class SerializableResourcePhone < JSONAPI::Serializable::Resource
  type 'resource_phones'

  attribute :resource_type

  # Flags
  # attribute :flags
  attribute :primary do
    @object.primary?
  end
  attribute :home do
    @object.home?
  end
  attribute :office do
    @object.office?
  end
  attribute :mobile do
    @object.mobile?
  end
  attribute :fax do
    @object.fax?
  end

  attribute :created_at
  attribute :updated_at

  has_one :main_phone
  has_one :phone
  has_one :resource
  has_one :agent
  has_many :home_phones
  has_many :office_phones
  has_many :mobile_phones
  has_many :fax_phones

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class SerializableLocation < JSONAPI::Serializable::Resource
  type 'locations'

  attribute :locatable_type
  attribute :locatable_id
  attribute :position_id

  attribute :created_at
  attribute :updated_at

  belongs_to :locatable
  belongs_to :position

  meta do
    meta_current
  end
end

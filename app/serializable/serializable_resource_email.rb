# frozen_string_literal: true

class SerializableResourceEmail < JSONAPI::Serializable::Resource
  type 'resource_emails'

  attribute :resource_type

  # Flags
  attribute :primary do
    @object.primary?
  end
  attribute :validated do
    @object.validated?
  end
  attribute :personal do
    @object.personal?
  end

  attribute :created_at
  attribute :updated_at

  belongs_to :email
  belongs_to :resource
  has_one :agent
  has_one :main_email

  meta do
    meta_current
  end
end

# frozen_string_literal: true

class APIController < ApplicationController
  def index
    skip_policy_scope
    @index = {
      data: [
        {
          reason: I18n.t('api.index'),
          api_key: INCOMMON::API_KEY_PUBLIC,
        },
      ],
      links: [
        { related: api_doc_url },
        { related: api_adm_url },
      ],
      meta: {
        api_key: {
          current: params[:api_key],
          hint: api_hint,
        },
        current: {
          agent: {
            name: current_user.name,
            uuid: current_user.uuid,
            link: {
              self: api_agent_url(current_user.uuid),
            },
          },
          user: {
            username: active_user.safe_username,
            api_key: active_user.api_token,
            uuid: active_user.uuid,
            anonymous: active_user.anonymous?,
            restricted: active_user.restricted?,
            link: {
              self: api_user_url(active_user.uuid),
            },
          },
          roles: {
            observer: current_user.observer?,
            editor: current_user.editor?,
            maintainer: current_user.maintainer?,
            leader: current_user.leader?,
          },
        },
        locale: {
          current: I18n.locale,
          default: I18n.default_locale,
          hint: I18n.t('api.help.locale_and_i18n'),
          link: {
            related: 'https://talk.incommon.cc/c/api/i18n',
          },
        },
      },
      jsonapi: { version: '1.0' },
    }
    render json: @index, content_type: 'application/vnd.api+json'
  end

  private

  def api_adm_url
    if Rails.env.production?
      'https://adm.incommon.cc/'
    else
      'https://adm.example.net/'
    end
  end

  def api_doc_url
    if Rails.env.production?
      'https://doc.incommon.cc/'
    else
      'http://doc.example.net/'
    end
  end

  def api_hint
    if params[:api_key].present?
      I18n.t('api.help.remember_to_use_api_key')
    else
      I18n.t('api.help.retry_with_api_key', url: root_url(api_key: INCOMMON.api_key))
    end
  end
end

# frozen_string_literal: true

class API::V0::LinksController < ApplicationController
  def index
    @links = policy_scope(Link)
    render jsonapi: @links,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @link = policy_scope(Link).find(params[:id])
    authorize @link
    render jsonapi: @link,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(resource_links).freeze
  end
end

# frozen_string_literal: true

class API::V0::SectionsController < ApplicationController
  def index
    @sections = policy_scope(Section)
    render jsonapi: @sections,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @section = policy_scope(Section).find(params[:id])
    authorize @section
    render jsonapi: @section,
           include: include_from_params,
           fields: params[:fields]
  end

  private

  def model_types
    %w(taxonomy category category.taxonomy resources
       agents entities places services things).freeze
  end
end

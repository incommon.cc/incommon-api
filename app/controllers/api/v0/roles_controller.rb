# frozen_string_literal: true

class API::V0::RolesController < ApplicationController
  def index
    @roles = policy_scope(Role)
    render jsonapi: @roles,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @role = policy_scope(Role).find(params[:id])
    authorize @role
    render jsonapi: @role,
           include: include_from_params,
           fields: params[:fields]
  end

  private

  def model_types
    %w(agent user).freeze
  end
end

# frozen_string_literal: true

class API::V0::PositionsController < ApplicationController
  def index
    @positions = policy_scope(Position)
    render jsonapi: @positions,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @position = policy_scope(Position).find(params[:id])
    authorize @position
    render jsonapi: @position,
           include: include_from_params,
           fields: params[:fields]
  end

  private

  def model_types
    %w(locations).freeze
  end
end

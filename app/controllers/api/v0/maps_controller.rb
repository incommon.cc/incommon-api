# frozen_string_literal: true

class API::V0::MapsController < ApplicationController
  def index
    @maps = policy_scope(Map).includes(:collection).all
    render jsonapi: @maps,
           include: include_from_params
  end

  def show
    @map = policy_scope(Map).includes(:collection, :taxonomy).find_by(uuid: params[:id])
    authorize @map
    render jsonapi: @map,
           include: include_from_params
  end

  private

  def model_types
    %w(agent collection resources position
       taxonomy taxonomy.categories taxonomy.sections).freeze
  end
end

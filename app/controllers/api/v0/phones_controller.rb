# frozen_string_literal: true

class API::V0::PhonesController < ApplicationController
  def index
    @phones = policy_scope(Phone)
    render jsonapi: @phones,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @phone = policy_scope(Phone).find(params[:id])
    authorize @phone
    render jsonapi: @phone,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(resource_phones).freeze
  end
end

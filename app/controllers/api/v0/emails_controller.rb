# frozen_string_literal: true

class API::V0::EmailsController < ApplicationController
  def index
    @emails = policy_scope(Email)
    render jsonapi: @emails,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @email = policy_scope(Email).find(params[:id])
    authorize @email
    render jsonapi: @email,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(resource_emails).freeze
  end
end

# frozen_string_literal: true

class API::V0::TaxonomiesController < ApplicationController
  def index
    @taxonomies = policy_scope(Taxonomy)
    render jsonapi: @taxonomies,
           fields: params[:fields],
           include: include_from_params
    # expose: [current_user, current_agent]
  end

  def show
    @taxonomy = policy_scope(Taxonomy).find_by(uuid: params[:id])
    authorize @taxonomy
    render jsonapi: @taxonomy,
           include: include_from_params,
           fields: params[:fields],
           links: { agent: api_agent_url(@taxonomy.agent) }
  end

  private

  def model_types
    %w(agent categories sections).freeze
  end
end

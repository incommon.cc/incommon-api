# frozen_string_literal: true

class API::V0::UsersController < ApplicationController
  # GET /users
  def index
    @users = policy_scope(User)
    render jsonapi: @users,
           include: include_from_params
  end

  # GET /users/:id
  def show
    @user = policy_scope(User).find_by(uuid: params[:id])
    authorize @user
    render jsonapi: @user,
           include: include_from_params
  end

  def model_types
    %w(agents roles).freeze
  end
end

# frozen_string_literal: true

class API::V0::ResourcesController < ApplicationController
  def index
    @resources = policy_scope(Resource)
    render jsonapi: @resources,
           include: include_from_params
  end

  def show
    @resource = policy_scope(Resource).find_by(uuid: params[:id])
    authorize @resource
    extra_links = { agent: api_agent_url(@resource.agent) } if @resource&.agent&.present?
    render jsonapi: @resource,
           include: include_from_params,
           fields: params[:fields],
           links: extra_links
  end

  private

  def model_types
    %w(agent
       collections
       locations
       sections
       resource_addresses
       resource_emails
       resource_links
       resource_phones
       addresses
       emails
       links
       phones).freeze
  end
end

# frozen_string_literal: true

class API::V0::AddressesController < ApplicationController
  def index
    @addresses = policy_scope(Address)
    render jsonapi: @addresses,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @address = policy_scope(Address).find(params[:id])
    authorize @address
    render jsonapi: @address,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(resource_addresses).freeze
  end
end

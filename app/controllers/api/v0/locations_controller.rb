# frozen_string_literal: true

class API::V0::LocationsController < ApplicationController
  def index
    @locations = policy_scope(Location)
    render jsonapi: @locations,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @location = policy_scope(Location).find(params[:id])
    authorize @location
    render jsonapi: @location,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(locatables positions)
  end
end

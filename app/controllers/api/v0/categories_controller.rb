# frozen_string_literal: true

class API::V0::CategoriesController < ApplicationController
  def index
    @categories = policy_scope(Category)
    render jsonapi: @categories,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @category = policy_scope(Category).find(params[:id])
    authorize @category
    render jsonapi: @category,
           fields: params[:fields],
           include: include_from_params
  end

  private

  def model_types
    %w(taxonomy sections).freeze
  end
end

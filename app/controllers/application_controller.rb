# frozen_string_literal: true

class ApplicationController < ActionController::API
  # INCOMMON::API::Controller implements an ActiveSupport::Concern to handle API
  # authorization and error handling for the whole application.
  # @see app/lib/incommon/api/controller.rb
  include INCOMMON::API::Controller
  # Pundit implements authorization policies.
  # @see https://github.com/varvet/pundit
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  if Rails.env.development?
    # @see https://github.com/varvet/pundit#ensuring-policies-and-scopes-are-used
    with_options if: proc { params[:pundit] == 'verify' } do
      # rubocop:disable Rails/LexicallyScopedActionFilter
      after_action :verify_authorized, except: :index
      after_action :verify_policy_scoped, only: :index
      # rubocop:enable Rails/LexicallyScopedActionFilter
    end
  end

  private

  def user_not_unauthorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    render jsonapi_errors: {
      title: I18n.t('pundit.authorization_error'),
      detail: I18n.t("#{policy_name}.#{exception.query}", scope: :pundit, default: :default),
      links: {
        about: policy_exception_doc_url(policy_name),
      },
    }, status: :forbidden
  end
end

# frozen_string_literal: true

class ErrorsController < ApplicationController
  def not_found
    raise INCOMMON::API::Errors::NotFoundError, I18n.t('api.help.not_found', path: params[:path])
  end
end

# frozen_string_literal: true

class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user   = user
    @record = record
  end

  def index?
    false
  end

  def show?
    false
  end

  def create?
    user.editor?
  end

  # We don't have #new in API mode
  def new?
    false
  end

  def update?
    create?
  end

  # We don't have #edit in API mode
  def edit?
    false
  end

  def destroy?
    user.maintainer?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  protected

  def incommon_agent?
    user.current_agent_is_incommon?
  end

  def record_public?
    record&.visible? == true
  end
end

# frozen_string_literal: true

class LinkPolicy < ApplicationPolicy
  def index
    true
  end

  def show
    true
  end
end

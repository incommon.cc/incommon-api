# frozen_string_literal: true

class PositionPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end
end

# frozen_string_literal: true

class EmailPolicy < ApplicationPolicy
  def index?
    user.leader? || user.maintainer?
  end

  def show?
    true
  end
end

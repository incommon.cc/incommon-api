# frozen_string_literal: true

class AgentPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    record == user.agent || record_public?
  end

  class Scope < Scope
    def resolve
      if user.current_agent_is_incommon? || user.leader? || user.maintainer?
        scope.all
      else
        scope.where(visible: true)
      end
    end
  end
end

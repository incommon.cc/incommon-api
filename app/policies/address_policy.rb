# frozen_string_literal: true

class AddressPolicy < ApplicationPolicy
  def index?
    user.maintainer?
  end

  def show?
    true
  end
end

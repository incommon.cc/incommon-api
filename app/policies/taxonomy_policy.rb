# frozen_string_literal: true

class TaxonomyPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end
end

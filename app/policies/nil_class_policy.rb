# frozen_string_literal: true

class NilClassPolicy < ApplicationPolicy
  def show?
    false # Nobody can see anything
  end

  class Scope < Scope
    def resolve
      raise Pundit::NotDefinedError, 'Cannot scope NilClass'
    end
  end
end

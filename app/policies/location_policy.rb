# frozen_string_literal: true

class LocationPolicy < ApplicationPolicy
  def index?
    user.maintainer?
  end

  def show?
    true
  end
end

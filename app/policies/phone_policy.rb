# frozen_string_literal: true

class PhonePolicy < ApplicationPolicy
  def index?
    user.maintainer?
  end

  def show?
    true
  end
end

# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def index?
    user&.leader? == true
  end

  def show?
    user&.leader? || user&.user == record
  end

  class Scope < Scope
    def resolve
      if !current_user&.restricted? && user&.leader?
        scope.all
      else
        scope.where(uuid: current_user&.uuid || User::ANONYMOUS_USER_ID)
      end
    end

    private

    def current_user
      user&.user&.presence
    end
  end
end

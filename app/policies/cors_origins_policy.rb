# frozen_string_literal: true

CORSOriginsPolicy = Struct.new(:user, :origin)

class CORSOriginsPolicy
  attr_reader :user, :origin

  def initialize(user, origin)
    @user = user
    @origin = origin
  end

  def index?
    true
  end

  def show?
    true
    # user.agent.origins.where(url: URI.parse(origin)).exists?
  end

  def create?
    user.maintainer?
  end
  alias update? create?

  def destroy?
    user.maintainer?
  end
end

# frozen_string_literal: true

class RolePolicy < ApplicationPolicy
  def index?
    user.leader?
  end

  def show?
    true
  end
end

# frozen_string_literal: true

class ResourcePolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    record_public?
  end

  class Scope < Scope
    def resolve
      if user.authenticated? == true
        scope.where(agent_id: user.uuid).or(scope.where(visible: true))
      else
        scope.where(visible: true)
      end
    end
  end
end

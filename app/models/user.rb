# frozen_string_literal: true

require_dependency 'incommon'

class User < ApplicationRecord
  include Encryptable
  include Hashable
  include UUIDParameter

  has_many :roles, dependent: :destroy
  has_many :agents, through: :roles

  # We must skip validations here if we want to auto-generate password
  has_secure_password validations: false
  has_secure_token              # nonce
  has_secure_token :auth_token  # regenerated at each login
  hashable :username
  encrypted_attributes :username

  validates :username,
            presence: true,
            uniqueness: true,
            format: { with: URI::MailTo::EMAIL_REGEXP }

  # We reproduce password validations from has_secure_password
  before_validation :ensure_password_is_set, on: :create
  validates :password,
            length: { maximum: ActiveModel::SecurePassword::MAX_PASSWORD_LENGTH_ALLOWED },
            confirmation: { allow_blank: true }

  # When a User is restricted, it cannot do certain things (see app/policies)
  # Use #restrict! to restrict a User at runtime (e.g., when authenticating insecurely)
  attr_reader :restricted
  alias restricted? restricted

  def initialize(*attributes)
    super
    @authenticated = false
    @restricted = false
  end

  # Override authenticate method to allow authorization policies for checking
  # whether the user has authenticated already or not.
  def authenticate(password)
    user = super(password)
    @authenticated = user.present?
    user.presence
  end

  def authenticated?
    @authenticated == true
  end

  def restrict!
    Rails.logger.info "Called restrict! on #{username}"
    @restricted = true
    self
  end

  # User.anonymous_user is a special User with a negative id.
  ANONYMOUS_USER_ID = -1

  def anonymous?
    id == ANONYMOUS_USER_ID
  end

  def self.anonymous_user
    find(ANONYMOUS_USER_ID)&.restrict!
  end

  def generate_api_token
    update_attribute(:api_token, SecureRandom.uuid)
  end

  def safe_username
    if restricted? && !anonymous?
      I18n.t('redacted')
    else
      username
    end
  end

  private

  # When creating the user, it may come from an external source and not have a
  # password set. In this case, we assign a super strong password because it's
  # probably not going to be used any time soon. We make it random so you,
  # cracker, have a hard time breaking it. Initial tokens are automatically
  # generated for us by has_secure_token. We do not, though, assign an API key
  # yet, since we don't know whether the user will use it: let them ask for it!
  def ensure_password_is_set
    self.password = SecureRandom.hex(rand(24..36)) if password_digest.blank?
    nil # Avoid leaking a secret
  end
end

# == Schema Information
#
# Table name: users
#
#  id              :bigint(8)        not null, primary key
#  api_token       :uuid
#  auth_token      :string
#  password_digest :string
#  sha256_hash     :string(64)
#  token           :string
#  username        :string
#  uuid            :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

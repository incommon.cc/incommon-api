# frozen_string_literal: true

class Map < ApplicationRecord
  # Universally Unique Identifier :uuid
  include UUIDParameter

  belongs_to :taxonomy
  belongs_to :position
  belongs_to :collection
  belongs_to :agent

  has_many :resource_collections, through: :collection, inverse_of: :collection
  with_options through: :resource_collections, source: :resource do
    has_many :resources
  end

  validates :zoom,
            presence: true,
            inclusion: {
              within: 1..18,
              message: I18n.t('.wrong_value'),
            }

  def center
    [longitude, latitude]
  end

  def latitude
    position&.latitude&.to_f
  end

  def longitude
    position&.longitude&.to_f
  end
end

# == Schema Information
#
# Table name: maps
#
#  id            :bigint(8)        not null, primary key
#  uuid          :uuid
#  zoom          :integer          default(13)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  agent_id      :bigint(8)
#  collection_id :bigint(8)
#  position_id   :bigint(8)
#  taxonomy_id   :bigint(8)
#
# Indexes
#
#  index_maps_on_collection_id  (collection_id)
#  index_maps_on_position_id    (position_id)
#  index_maps_on_taxonomy_id    (taxonomy_id)
#  index_maps_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (collection_id => collections.id)
#  fk_rails_...  (position_id => positions.id)
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#

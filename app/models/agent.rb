# frozen_string_literal: true

class Agent < Entity
  INCOMMON_AGENT_ID = -1

  with_options dependent: :destroy do
    has_many :collections
    has_many :taxonomies
    has_many :resources, inverse_of: :agent
    has_many :roles
  end
  has_many :users, through: :roles
  with_options through: :roles, source: :user, class_name: 'User' do
    has_many :editors,     ->(agent) { agent.roles.editor }
    has_many :maintainers, ->(agent) { agent.roles.maintainer }
    has_many :observers,   ->(agent) { agent.roles.observer }
    has_many :leaders,     ->(agent) { agent.roles.leader }
  end

  # The :resource_links association is defined in Resource
  has_many :origins, through: :resource_links, source: :link

  def agent
    self
  end

  def default_roles
    Role.find_by(user: User.anonymous_user, agent: Agent.incommon_agent)
  end

  def self.incommon_agent
    find(INCOMMON_AGENT_ID)
  end
end

# == Schema Information
#
# Table name: resources
#
#  id           :bigint(8)        not null, primary key
#  meta         :json
#  translations :json
#  type         :string(16)       default("Resource")
#  uuid         :uuid
#  visible      :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_resources_on_agent_id  (agent_id)
#

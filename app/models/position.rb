# frozen_string_literal: true

#
# Position may be confusing since its tries to be many things.
#
# The main information is stored in #geometry; #geometry can store any RGeo
# (GeoJSon geometry) types: Point, MultiPoint, LineString, etc.
#
# The rest of the information is indicative and may differ from the actual type
# of the #geometry. For example, take a Point:
#
# - #geometry stores its longitude and latitude
# - #geo_type SHOULD be 'Point'
# - #box and #radius may or may not be indicated.
# - the record may or may not have information at #longitude, #latitude, or
#   #elevation, and they may or may not match what's in #geometry (normally,
#   they would, but it's not guaranteed.)
#
# In the case of a MultiPoint or Polygon...
#
# - #geometry stores the coordinates and type
# - #geo_type SHOULD be 'MultiPoint'
# - if any of #box, #longitude, #latitude, or #radius are indicated, they
#   SHOULD refer to the center of the area covered by the record.
#
# However there's no guarantee (yet) that this is the case.
# Better rely on the #geometry element and create the rest as needed.
#
class Position < ApplicationRecord
  # Valid GeoJSON geometry types: https://tools.ietf.org/html/rfc7946#section-3.1.1
  GEO_TYPES = %w(Point MultiPoint LineString MultiLineString Polygon MultiPolygon).freeze

  before_save :update_latlon, if: :point_geometry_changed?

  has_many :locations, dependent: :destroy
  with_options through: :locations, source: :locatable do
    has_many :agents,   source_type: 'Agent'
    has_many :entities, source_type: 'Entity'
    has_many :places,   source_type: 'Place'
    has_many :services, source_type: 'Service'
    has_many :things,   source_type: 'Thing'
  end

  validates :geo_type,
            presence: true,
            inclusion: { in: GEO_TYPES }

  with_options if: :point?, allow_nil: false do
    validates :latitude,
              inclusion: { within: -90..90 }
    validates :longitude,
              inclusion: { within: -180..180 }
  end
  with_options unless: :point?, allow_nil: true do
    validates :latitude,
              inclusion: { within: -90..90 }
    validates :longitude,
              inclusion: { within: -180..180 }
  end

  validates :radius,
            allow_nil: true,
            numericality: { greater_than: 0 }

  def lat
    format('%2.7f', (point? ? geometry.y : latitude))
  end

  def latitude=(value)
    super(format('%2.7f', value))
  end

  def lon
    format('%3.7f', (point? ? geometry.x : longitude))
  end

  def longitude=(value)
    super(format('%3.7f', value))
  end

  def elevation=(value = 0)
    super(format('%5.2f', value || 0))
  end

  def point?
    geo_type == 'Point'
  end

  private

  def point_geometry_changed?
    point? && (geometry_changed? || latitude_changed? || longitude_changed?)
  end

  def update_latlon
    if geometry.present?
      lon = format('%2.7f', geometry.x)
      lat = format('%3.7f', geometry.y)
    elsif longitude.present? && latitude.present?
      lon = format('%2.7f', longitude)
      lat = format('%3.7f', latitude)
    end
    unless lon.blank? || lat.blank?
      self[:geometry]  = "POINT(#{lon} #{lat})"
      self[:longitude] = lon
      self[:latitude]  = lat
    end
  end
end

# == Schema Information
#
# Table name: positions
#
#  id         :bigint(8)        not null, primary key
#  box        :json
#  elevation  :decimal(7, 2)
#  geo_type   :string
#  geometry   :geometry({:srid= geometry, 0
#  latitude   :decimal(9, 7)
#  longitude  :decimal(10, 7)
#  radius     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_positions_on_geometry  (geometry) USING gist
#

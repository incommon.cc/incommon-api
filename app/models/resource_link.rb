# frozen_string_literal: true

class ResourceLink < ApplicationRecord
  include Bitfields

  bitfield :flags, :homepage, :icon, :origin

  belongs_to :link
  belongs_to :resource, polymorphic: true
  with_options inverse_of: :resource_links do
    belongs_to :agent,
               -> { where(type: 'Agent') },
               class_name: 'Agent',
               foreign_key: :resource_id,
               optional: true
    has_many :origins,
             -> { origin },
             class_name: 'Link'
  end

  scope :icon,    -> { icon }
  scope :origin,  -> { origin }
  scope :primary, -> { homepage }
end

# == Schema Information
#
# Table name: resource_links
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  link_id       :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_links_on_link_id                        (link_id)
#  index_resource_links_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (link_id => links.id)
#

# frozen_string_literal: true

class Location < ApplicationRecord
  belongs_to :position
  belongs_to :locatable, polymorphic: true
end

# == Schema Information
#
# Table name: locations
#
#  id             :bigint(8)        not null, primary key
#  locatable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  locatable_id   :bigint(8)
#  position_id    :bigint(8)
#
# Indexes
#
#  index_locations_on_locatable_type_and_locatable_id  (locatable_type,locatable_id)
#  index_locations_on_position_id                      (position_id)
#
# Foreign Keys
#
#  fk_rails_...  (position_id => positions.id)
#

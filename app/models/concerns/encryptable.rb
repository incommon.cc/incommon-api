# frozen_string_literal: true

# Make Rails 5 model attributes encryptable.
# Kudos to @aloucas and @MirMayne for inspiration.
# app/models/concerns/encryptable.rb
module Encryptable
  extend ActiveSupport::Concern

  included do
    class_attribute :encrypted_columns, instance_writer: false, default: []
  end

  class_methods do
    def encrypted_attributes(*attributes)
      attributes.each do |attr_name|
        encrypted_columns << attr_name

        define_method(attr_name) do
          Encryptable.crypt.decrypt_and_verify(self[attr_name]) if attribute_present?(attr_name)
        rescue ActiveSupport::MessageEncryptor::InvalidMessage
          warn 'Decryption failed'
          raise
        end

        define_method("#{attr_name}=") do |value|
          super(value.present? ? Encryptable.crypt.encrypt_and_sign(value) : value)
        end
      end
    end
  end

  # Create the base MessageEncryptor based on Rails secret_key_base
  # Defaults to Rails.application.secret_key_base, but uses
  # credentials if provided in development and test environments
  def self.crypt
    secret_key_base = if use_credentials?
                        Rails.application.credentials.secret_key_base
                      else
                        Rails.application.secret_key_base
                      end
    ActiveSupport::MessageEncryptor.new(secret_key_base[0...32])
  end

  private

  def self.use_credentials?
    !Rails.env.production? &&
      !Rails.application.credentials.secret_key_base.nil?
  end
end

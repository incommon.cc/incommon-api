# frozen_string_literal: true

# = Hashable =
#
# This module adds the possibility to store a SHA256 hash of one or more values
# in a model.
#
# It add a :hashable_columns class attribute to indicate which columns to
# include in the hash. The hash is automatically generated before each save to
# update the hash where the values changed. The hash is stored in a :sha256_hash
# column inside the model (you need to create it first), or a custom column
# passed from arguments:
#
#    class Email < ApplicationRecord
#      include Hashable
#
#      hashable :address, column: :hashed_email
#    end
#
#    e = Email.create('foo@example.net')
#    e.hashed_email # => "8886dbe0fed71dfd6af86fcba079e21f98c839446596af86ccb4e75feb79aa3b"
#
# This serves to lookup values without having to use the actual value, e.g.,
# when you want to protect emails from spambots, or if you wish to implement
# something like Libravatar, using the hash instead of the clear text
# representation.

module Hashable
  extend ActiveSupport::Concern

  class_methods do
    # Register list of attributes to hash into the `column' attribute.
    # Ensure `column' is not part of these! (Default is :sha256_hash.)
    # Define the before_save filter to update the hash
    def hashable(*attributes)
      options = attributes.extract_options!
      self.hashable_column_name = (options[:column] || :sha256_hash).to_sym

      attributes.each do |attr_name|
        name = attr_name.to_sym
        hashable_columns << name unless name == hashable_column_name
      end
    end
  end

  included do
    with_options instance_writer: false do
      class_attribute :hashable_column_name, default: :sha256_hash
      class_attribute :hashable_columns, default: []
    end
    before_save :hexdigest_hashable_columns

    def hexdigest_hashable_columns
      value = hashable_columns.map { |attr| (send(attr) || '').to_s }.join('|')
      self[hashable_column_name] = Digest::SHA256.hexdigest(value)
    end
  end
end

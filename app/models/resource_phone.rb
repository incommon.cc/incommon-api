# frozen_string_literal: true

class ResourcePhone < ApplicationRecord
  include Bitfields

  bitfield :flags, :primary, :home, :office, :mobile, :fax

  belongs_to :phone
  belongs_to :resource, polymorphic: true
  with_options inverse_of: :resource_phones do
    belongs_to :agent,
               -> { where(resource_type: 'Agent') },
               class_name: 'Agent',
               optional: true
    with_options class_name: 'Phone' do
      has_one  :main_phone,    -> { primary }
      has_many :home_phones,   -> { home }
      has_many :office_phones, -> { office }
      has_many :mobile_phones, -> { mobile }
      has_many :fax_phones,    -> { fax }
    end
  end

  def to_s
    main_phone.&number
  end

  scope :fax,     -> { where(bitfield_sql(fax: true)) }
  scope :home,    -> { where(bitfield_sql(home: true)) }
  scope :mobile,  -> { where(bitfield_sql(mobile: true)) }
  scope :office,  -> { where(bitfield_sql(office: true)) }
  scope :primary, -> { where(bitfield_sql(primary: true)) }

  # TODO: this breaks, reclaiming a 'resource_phones.resource_phone_id' key...
  # See: https://github.com/rails/rails/issues/20676
  # validates :main_phone,
  #           uniqueness: { scope: :resource }
end

# == Schema Information
#
# Table name: resource_phones
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  phone_id      :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_phones_on_phone_id                       (phone_id)
#  index_resource_phones_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (phone_id => phones.id)
#

# frozen_string_literal: true

class Taxonomy < ApplicationRecord
  # I18n :translations
  extend Mobility
  translates :name, :summary, :description
  # Universally Unique Identifier :uuid
  include UUIDParameter

  default_scope { order(created_at: :asc) }

  belongs_to :agent
  has_many :categories, -> { order(rank: :asc) }, dependent: :destroy, inverse_of: :taxonomy
  has_many :sections, -> { order(rank: :asc) }, through: :categories

  validates :name,
            presence: true,
            uniqueness: true,
            length: 3..64
  validates :summary,
            length: 0..136
end

# == Schema Information
#
# Table name: taxonomies
#
#  id               :bigint(8)        not null, primary key
#  categories_count :integer          default(0)
#  translations     :json
#  uuid             :string(36)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  agent_id         :bigint(8)
#
# Indexes
#
#  index_taxonomies_on_agent_id  (agent_id)
#  index_taxonomies_on_uuid      (uuid) UNIQUE
#

# frozen_string_literal: true

class Phone < ApplicationRecord
  include Bitfields

  bitfield :flags, :failing

  has_many :resource_phones, dependent: :destroy
  with_options through: :resource_phones, source: :resource do
    has_many :resources, source_type: 'Resource'
    has_many :entities,  source_type: 'Entity'
    has_many :places,    source_type: 'Place'
    has_many :services,  source_type: 'Service'
    has_many :things,    source_type: 'Thing'
  end

  phony_normalize :number, default_country_code: 'BE'

  validates :number,
            presence: true,
            phony_plausible: { enforce_record_country: false },
            format: { with: /\A\+\d+\z/ }

  def to_s
    number.phony_formatted(format: :international)
  end
end

# == Schema Information
#
# Table name: phones
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  number     :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_phones_on_number  (number) UNIQUE
#

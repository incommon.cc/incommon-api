# frozen_string_literal: true

class Email < ApplicationRecord
  include Bitfields
  include Encryptable
  include Hashable

  bitfield :flags, :bouncing, :failing

  has_many :resource_emails, dependent: :destroy
  with_options through: :resource_emails, source: :resource do
    has_many :resources, source_type: 'Resource'
    has_many :entities,  source_type: 'Entity'
    has_many :places,    source_type: 'Place'
    has_many :services,  source_type: 'Service'
    has_many :things,    source_type: 'Thing'
  end

  has_many :user_emails, dependent: :destroy
  has_many :users, through: :user_emails, source: :user

  encrypted_attributes :address
  hashable :address

  validates :address,
            presence: true,
            uniqueness: true,
            format: { with: URI::MailTo::EMAIL_REGEXP }

  def to_s
    address
  end

  class << self
    def with_address!(address)
      find_by("#{hashable_column_name}": EmailHelper.hash_email(address)) ||
        create!(address: address)
    end
  end
end

# == Schema Information
#
# Table name: emails
#
#  id          :bigint(8)        not null, primary key
#  address     :string
#  flags       :integer          default(0), not null
#  sha256_hash :string(64)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

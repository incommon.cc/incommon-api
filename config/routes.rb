# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'api#index', via: [:get]

  # INCOMMON::API.routes
  namespace :api, path: '' do
    scope module: :v0,
          constraints: INCOMMON::API::Constraints.new(version: 0, default: true) do
      # High-level routes: these can be called with UUID
      # - Agents: Collections, Roles
      # - Maps: Position
      # - Resources (and: Entities, Places, Services, Things): Locations, Resource*
      # - Taxonomies: Categories, Sections
      # - Users

      # Classifications
      resources :taxonomies
      resources :categories
      resources :sections

      # Collections
      resources :collections

      # Descriptors

      # Commoners
      resources :agents
      resources :roles
      resources :users

      # Maps
      resources :maps
      resources :positions

      # Properties
      resources :addresses
      resources :emails
      resources :links
      resources :phones

      # Resources
      resources :resources, only: [:index, :show]
      resources :entities
      resources :places
      resources :things
    end

  #   scope module: :v1, constraints: INCOMMON::API::Constraints.new(version: 1) do
  #     # Descriptors
  #     resources :moments
  #     resources :tags
  #
  #     # Resources
  #     resources :services
  #   end
  #
  #   scope module: :v2, constraints: INCOMMON::API::Constraints.new(version: 2) do
  #     # Descriptors
  #     resources :events
  #   end
  end

  # Return JSON 404 errors in production for all unknown routes
  unless Rails.application.config.consider_all_requests_local
    match '*path', to: 'errors#not_found', via: :all
  end


  # For details on the DSL available within this file, see
  # http://guides.rubyonrails.org/routing.html
end

Rails.autoloaders.each do |autoloader|
  autoloader.inflector.inflect(
    "incommon" => "INCOMMON",
    "jsonapi_serializable_helper" => "JSONAPISerializableHelper"
  )
end

# frozen_string_literal: true

# See lib/sso/from_discourse.rb
# module SSO
#   class FromDiscourse
#     class << self
#       attr_accessor :config
#     end
#   end
# end

require_dependency 'sso'

SSO::FromDiscourse.config = {
  sso_url: 'https://talk.incommon.cc/session/sso_provider',
  return_url: "#{Rails.configuration.x.incommon.api_root_url}/authenticate",
  sso_secret: Rails.application.credentials.sso_secret,
}

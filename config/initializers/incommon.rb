# frozen_string_literal: true

class IncommonApi::Application
  # Vary root_url domain according to environment.
  # In production: api.incommon.cc
  # Otherwise:     api.example.net
  config.x.incommon.api_root_url = 'https://api.%{domain}' % \
  { domain: Rails.env.production? ? 'incommon.cc' : 'example.net' }
end

# Now that we pass `@current_user` to JSONAPI serializers, we can make use of
# our authorization helpers...
#
# @see config/initializers/jsonapi.rb
# @see app/lib/incommon/api/jsonapi.rb
#
[:Link, :Relationship, :Resource].each do |klass|
  JSONAPI::Serializable.const_get(klass).include INCOMMON::API::JSONAPISerializableHelper
end

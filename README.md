# IN COMMON API

1. [Introduction & Usage](#intro)
2. [Installation & Configuration](#install)
3. [Contributing & Reporting](#contribute)
4. [Licensing](#license)
5. [Community](https://talk.incommon.cc) & [Support](https://talk.incommon.cc/c/api/support)


<a name="intro"></a>

## [Introduction](https://api.incommon.cc/intro) & Usage

[![pipeline status](https://gitlab.com/incommon.cc/incommon-api/badges/master/pipeline.svg)](https://gitlab.com/incommon.cc/incommon-api/commits/master)

[![coverage report](https://gitlab.com/incommon.cc/incommon-api/badges/master/coverage.svg)](https://gitlab.com/incommon.cc/incommon-api/commits/master)

This is a Rails 6 API-only application using Ruby 2.6.5 (MRI) and PostgreSQL 11
with PostGIS extensions.

### Usage

**Always use an API key**. The default, public API key is: `23d8009f-f2ec-44b6-bcbe-58ad1d3af10b`.

In your requests to the IN COMMON API, you can use it as a query parameter:

``` shell
curl https://api.incommon.cc/?api_key=23d8009f-f2ec-44b6-bcbe-58ad1d3af10b
```


<a name="install"></a>

## Installation & Configuration

1. [System Dependencies](#system-dependencies)
2. [Configuration](#configuration)
3. [Testing](#testing)
4. [Services](#services)
5. [Deployment](#deployment)


<a name="system-dependencies"></a>

### System Dependencies

It is developped on Gentoo / GNU/Linux and should support any GNU/Linux system
in production. For Ruby, we recommend using [ruby-install] and [chruby]!

[ruby-install]: https://github.com/postmodern/ruby-install
[chruby]: https://github.com/postmodern/chruby


<a name="configuration"></a>

### Configuration

Examples show the case of Debian-based systems. <abbr title="Your Mileage May Vary">YMMV</abbr>.

#### NginX + Puma

Local development is setup to use Puma with sockets. 

If you don't have `sudo` access, consider changing Puma configuration from:

`bind "unix://#{shared_dir}/sockets/puma.sock";` to: `bind "tcp://127.0.0.1:3001/";`

And skip this step :)

See [doc/development/nginx.conf] for a sample developer configuration. With this
setup you should add an entry in your `/etc/hosts`:

``` shell
echo '127.0.0.1 api.example.net' | sudo tee -a /etc/hosts
```

[doc/development/nginx.conf]: ../doc/development/nginx.conf

#### PostgreSQL + PostGIS

We use PostgreSQL 11. Enable PostGIS extensions (or ask your system administrator to do so).

`sudo apt install postgresql-11 postgis`

Your current database user should be authorized in PostgreSQL. If not, you should enable it:

``` shell
cat <<DB_ACCESS | sudo tee -a $PATH_TO_PG_HBA_CONF
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   $INCOMMON_DB_NAME $INCOMMON_DB_USER                     md5
DB_ACCESS

```

In the above snippet...

| Replace                 | With...
|-|-
| `$INCOMMON_API_DB_NAME` | your actual database name (e.g., `incommon-api`)
| `$INCOMMON_API_DB_USER` | your actual database user name (e.g., `rails`)
| `$PATH_TO_PG_HBA_CONF`  | On Debian-based systems: `/etc/postgresql/10/main/pg_hba.conf`

Then reload PostgreSQL server: `sudo /etc/init.d/postgresql reload`

##### Loading the database

Please refer to [doc/database_setup.md].

[doc/database_setup.md]: ../doc/database_setup.md


<a name="testing"></a>

### Testing

We recommend using Guard: `bundle exec guard`.

You can run the specification suite with: `bundle exec rspec`.

Please refer to [RSpec documentation] to run or write individual tests.

[RSpec documentation]: http://rspec.info/documentation/3.8/rspec-core/


<a name="services"></a>

### Services

Writing to the database **will** use Job queues running [Sidekiq] (or similar).

[Sidekiq]: https://github.com/mperham/sidekiq


<a name="deployment"></a>

### Deployment

- always use `db/structure.sql` and database import to get started. It will save
  you some headaches.
- use sockets! They're faster and more secure.
- deploy version tags (`git tag -l | tail -1`)
- **always enable TLS**: the API assumes the connection to the server is
  secure. Not doing so will leak API keys and authentication tokens. You've been
  warned. [LetsEncrypt] gives you gratis SSL certificates.

[LetsEncrypt]: https://letsencrypt.org/


<a name="contribute"></a>

## Contributing & Reporting

We're a welcoming community. We expect every contributor to be respectful of
each other and the [IN COMMON Charter].

Our main development SCM goes to <https://framagit.org/incommon.cc/incommon-api>.

It is mirrored to [Gitlab] and [Github].

[IN COMMON Charter]: https://talk.incommon.cc/incommon-charter
[Gitlab]: https://gitlab.com/incommon.cc/incommon-api
[Github]: https://github.com/moners/incommon-api

Our development community gathers at <https://talk.incommon.cc/c/api>.

### Reporting Security Issues

See <https://talk.incommon.cc/t/welcome-developers/127/1>


<a name="license"></a>

## Licensing

`incommon-api` Copyright © 2018 [IN COMMON Collective] and contributors.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](../LICENSE.md)

[IN COMMON Collective]: https://talk.incommon.cc/groups/incommon

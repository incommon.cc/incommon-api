require 'rails_helper'

RSpec.describe CollectionPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions ".scope" do
    pending 'resolves to all'
  end

  permissions :index? do
    pending "is always allowed"
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

require 'rails_helper'

RSpec.describe LinkPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is always allowed'
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

require 'rails_helper'

RSpec.describe ResourceLinkPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows LinkPolicy' do
    expect(subject.ancestors[1]).to eql(LinkPolicy)
  end
end

require 'rails_helper'

RSpec.describe ResourcePolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is always allowed'
  end

  permissions :show? do
    pending 'is only allowed for visible resources'
  end

  permissions '.scope' do
    pending 'is only allowed for visible resources'
    context 'when authenticated' do
      pending 'is restricted to current agent\'s resources'
    end
  end
end

require 'rails_helper'

RSpec.describe SectionPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows Taxonomy policy' do
    expect(subject.ancestors[1]).to eql(TaxonomyPolicy)
  end
end

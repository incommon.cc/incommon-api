require 'rails_helper'

RSpec.describe ResourceAddressPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows AddressPolicy' do
    expect(subject.ancestors[1]).to eql(AddressPolicy)
  end
end

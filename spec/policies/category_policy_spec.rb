require 'rails_helper'

RSpec.describe CategoryPolicy do
  subject { described_class }

  it 'follows TaxonomyPolicy' do
    expect(subject.ancestors[1]).to eql(TaxonomyPolicy)
  end
end

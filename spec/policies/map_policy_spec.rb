require 'rails_helper'

RSpec.describe MapPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is always allowed'
  end

  permissions :show? do
    pending 'is allowed for public (visible) maps'
  end
end

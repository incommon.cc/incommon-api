require 'rails_helper'

RSpec.describe EmailPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is allowed for leader'
    pending 'is allowed for maintainer'
    pending 'is not allowed otherwise'
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

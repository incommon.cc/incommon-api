require 'rails_helper'

RSpec.describe ResourceEmailPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows EmailPolicy' do
    expect(subject.ancestors[1]).to eql(EmailPolicy)
  end
end

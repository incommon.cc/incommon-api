require 'rails_helper'

RSpec.describe ResourcePhonePolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows PhonePolicy' do
    expect(subject.ancestors[1]).to eql(PhonePolicy)
  end
end

require 'rails_helper'

RSpec.describe ResourceCollectionPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows CollectionPolicy' do
    expect(subject.ancestors[1]).to eql(CollectionPolicy)
  end
end

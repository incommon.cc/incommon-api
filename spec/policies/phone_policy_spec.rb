require 'rails_helper'

RSpec.describe PhonePolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'can be listed by maintainer'
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

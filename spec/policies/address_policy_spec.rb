require 'rails_helper'

RSpec.describe AddressPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is allowed to maintainer'
  end
  
  permissions :show? do
    pending 'is always allowed'
  end
end

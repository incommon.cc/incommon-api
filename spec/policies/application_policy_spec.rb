require 'rails_helper'

RSpec.describe ApplicationPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions '.scope' do
    pending 'resolves to all'
  end

  permissions :index? do
    pending 'is not allowed'
  end

  permissions :show? do
    pending 'is not allowed'
  end

  permissions :create? do
    pending 'is allowed to editor'
  end

  permissions :new? do
    pending 'is never allowed'
  end

  permissions :update? do
    pending 'follows create policy'
  end

  permissions :edit? do
    pending 'is never allowed'
  end

  permissions :destroy? do
    pending 'is allowed to maintainer'
  end  
end

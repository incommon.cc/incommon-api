require 'rails_helper'

RSpec.describe LocationPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'can be listed by maintainers'
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

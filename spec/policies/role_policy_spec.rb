require 'rails_helper'

RSpec.describe RolePolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    pending 'is only allowed to leader'
  end

  permissions :show? do
    pending 'is always allowed'
  end
end

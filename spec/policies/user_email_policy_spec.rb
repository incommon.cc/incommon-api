require 'rails_helper'

RSpec.describe UserEmailPolicy do
  let(:user) { User.new }

  subject { described_class }

  it 'follows Email policy' do
    expect(subject.ancestors[1]).to eql(EmailPolicy)
  end
end

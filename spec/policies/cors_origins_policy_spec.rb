require 'rails_helper'

RSpec.describe CORSOriginsPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions :index? do
    it 'is always allowed'
  end

  permissions :show? do
    it 'is always allowed'
    pending 'is only allowed for actual Agent\'s origins'
  end

  permissions :create? do
    pending 'is allowed to maintainer'
  end

  permissions :update? do
    pending 'follows create?'
  end

  permissions :destroy? do
    pending 'follows create?'
  end
end
  

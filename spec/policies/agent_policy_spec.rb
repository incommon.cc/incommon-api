require 'rails_helper'

RSpec.describe AgentPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions ".scope" do
    pending 'resolves to all if current agent is IN COMMON'
    pending 'resolves to all for leader'
    pending 'resolves to all for maintainer'
    pending 'resolves to visible records otherwise'
  end

  permissions :index? do
    pending 'is always allowed'
  end

  permissions :show? do
    pending 'is allowed if user belongs to agent'
    pending 'is allowed if record is public'
  end
end

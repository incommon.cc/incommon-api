require 'rails_helper'

RSpec.describe UserPolicy do
  let(:user) { User.new }

  subject { described_class }

  permissions ".scope" do
    pending 'resolves to all for leader'
    pending 'resolves to all for unrestricted user'
    pending 'resolves to self or anonymous otherwise'
  end

  permissions :index? do
    pending 'is only allowed to leader'
  end

  permissions :show? do
    pending 'is allowed to leader'
    pending 'is allowed to self'
  end
end

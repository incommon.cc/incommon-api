require 'rails_helper'

RSpec.describe EmailHelper do
  include EmailHelper

  describe '#hash_email' do
    it 'returns the SHA256 hash of the passed string' do
      expect(hash_email('foo@example.net'))
        .to eql(::Digest::SHA256.hexdigest('foo@example.net'))
    end
  end
end

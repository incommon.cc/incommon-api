# SPDX-FileCopyrightText: 2017-2019 INCOMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# frozen_string_literal: true

require 'rails_helper'

describe 'API routes' do
  let(:id) { 'a11f3120-1b96-4581-9c49-668af0f18a18' }
  
  describe 'classification routes' do
    it 'routes to taxonomies' do
      expect(get('/taxonomies')).
        to route_to('api/v0/taxonomies#index')
    end
    it 'routes to create a taxonomy' do
      expect(post("/taxonomies/")).
        to route_to('api/v0/taxonomies#create')
    end
    it 'routes to read a taxonomy' do
      expect(get("/taxonomies/#{id}")).
        to route_to('api/v0/taxonomies#show', id: id)
    end
    it 'routes to update a taxonomy' do
      expect(patch("/taxonomies/#{id}")).
        to route_to('api/v0/taxonomies#update', id: id)
    end
    it 'routes to destroy a taxonomy' do
      expect(delete("/taxonomies/#{id}")).
        to route_to('api/v0/taxonomies#destroy', id: id)
    end
  end
end

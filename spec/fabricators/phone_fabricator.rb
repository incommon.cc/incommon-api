Fabricator(:phone) do
  number "+32487012345"
  flags  1
end

# == Schema Information
#
# Table name: phones
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  number     :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_phones_on_number  (number) UNIQUE
#

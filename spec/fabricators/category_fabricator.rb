Fabricator(:category) do
  name        { Faker::Movies::PrincessBride.character[0...64] }
  summary     { Faker::Movies::PrincessBride.quote[0...136] }
  description { Faker::Markdown.sandwich(6, 3) }
  color       { Faker::Color.hex_color }
  taxonomy
end

# == Schema Information
#
# Table name: categories
#
#  id             :bigint(8)        not null, primary key
#  color          :string(25)
#  rank           :integer
#  sections_count :integer          default(0)
#  translations   :json
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  taxonomy_id    :bigint(8)
#
# Indexes
#
#  index_categories_on_taxonomy_id  (taxonomy_id)
#
# Foreign Keys
#
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#

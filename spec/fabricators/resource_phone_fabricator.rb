Fabricator(:resource_phone) do
  resource
  phone
  flags    0
end

# == Schema Information
#
# Table name: resource_phones
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  phone_id      :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_phones_on_phone_id                       (phone_id)
#  index_resource_phones_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (phone_id => phones.id)
#

Fabricator(:resource) do
  name        "MyString"
  summary     "MyString"
  description "MyText"
  meta        ""
  visible     false
  uuid        ""
  type        ""
end

Fabricator(:new_resource) do
  name        "A New Resource"
  summary     "It's missing a type"
  description nil
  meta        nil
  visible     false
  uuid        nil
  type        nil
end

# == Schema Information
#
# Table name: resources
#
#  id           :bigint(8)        not null, primary key
#  meta         :json
#  translations :json
#  type         :string(16)       default("Resource")
#  uuid         :uuid
#  visible      :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_resources_on_agent_id  (agent_id)
#

email_address = Faker::Internet.email

Fabricator(:email) do
  address     { email_address }
  sha256_hash { Digest::SHA256.hexdigest(email_address) }
  flags       0
end

# == Schema Information
#
# Table name: emails
#
#  id          :bigint(8)        not null, primary key
#  address     :string
#  flags       :integer          default(0), not null
#  sha256_hash :string(64)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

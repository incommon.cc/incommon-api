Fabricator(:resource_section) do
  id            1
  resource
  section
end

# == Schema Information
#
# Table name: resource_sections
#
#  id            :bigint(8)        not null, primary key
#  resource_type :string
#  resource_id   :bigint(8)
#  section_id    :bigint(8)
#
# Indexes
#
#  index_resource_sections_on_resource_type_and_resource_id  (resource_type,resource_id)
#  index_resource_sections_on_section_id                     (section_id)
#
# Foreign Keys
#
#  fk_rails_...  (section_id => sections.id)
#

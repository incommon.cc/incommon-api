Fabricator(:resource_collection) do
  resource
  collection
end

# == Schema Information
#
# Table name: resource_collections
#
#  id            :bigint(8)        not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  collection_id :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_collections_on_collection_id  (collection_id)
#  index_resource_collections_on_resource_id    (resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (collection_id => collections.id)
#  fk_rails_...  (resource_id => resources.id)
#

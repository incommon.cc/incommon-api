Fabricator(:position) do
  box       nil
  elevation "9.99"
  geo_type  "Point"
  geometry  "POINT (4.3510466 50.8378565)"
  latitude  "50.8378565"
  longitude "4.3510466"
  radius    nil
end

# == Schema Information
#
# Table name: positions
#
#  id         :bigint(8)        not null, primary key
#  box        :json
#  elevation  :decimal(7, 2)
#  geo_type   :string
#  geometry   :geometry({:srid= geometry, 0
#  latitude   :decimal(9, 7)
#  longitude  :decimal(10, 7)
#  radius     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_positions_on_geometry  (geometry) USING gist
#

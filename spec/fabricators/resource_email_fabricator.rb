Fabricator(:resource_email) do
  email    nil
  resource nil
  flags    1
end

# == Schema Information
#
# Table name: resource_emails
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  email_id      :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_emails_on_email_id                       (email_id)
#  index_resource_emails_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (email_id => emails.id)
#

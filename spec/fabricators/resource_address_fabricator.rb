Fabricator(:resource_address) do
  address  nil
  resource nil
  flags    1
end

# == Schema Information
#
# Table name: resource_addresses
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  address_id    :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_addresses_on_address_id                     (address_id)
#  index_resource_addresses_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#

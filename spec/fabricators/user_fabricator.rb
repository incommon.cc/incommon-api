Fabricator(:user) do
  username   "some_encrypted_string"
  password   "some_encrypted_string"
  auth_token "a_secure_token"
  nonce      "a_secure_token"
  api_token  { SecureRandom.uuid }
end

# == Schema Information
#
# Table name: users
#
#  id              :bigint(8)        not null, primary key
#  api_token       :uuid
#  auth_token      :string
#  password_digest :string
#  sha256_hash     :string(64)
#  token           :string
#  username        :string
#  uuid            :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

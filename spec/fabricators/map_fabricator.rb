Fabricator(:map) do
  agent
  taxonomy
  position
  collection
  zoom       10
  uuid       { SecureRandom.uuid }
end

# == Schema Information
#
# Table name: maps
#
#  id            :bigint(8)        not null, primary key
#  uuid          :uuid
#  zoom          :integer          default(13)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  agent_id      :bigint(8)
#  collection_id :bigint(8)
#  position_id   :bigint(8)
#  taxonomy_id   :bigint(8)
#
# Indexes
#
#  index_maps_on_collection_id  (collection_id)
#  index_maps_on_position_id    (position_id)
#  index_maps_on_taxonomy_id    (taxonomy_id)
#  index_maps_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (collection_id => collections.id)
#  fk_rails_...  (position_id => positions.id)
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#

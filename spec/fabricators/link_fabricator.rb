Fabricator(:link) do
  uri   { Faker::Internet.url }
  flags 0
end

# == Schema Information
#
# Table name: links
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  uri        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

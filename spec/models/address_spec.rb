require 'rails_helper'

RSpec.describe Address, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
#
# Table name: addresses
#
#  id          :bigint(8)        not null, primary key
#  country     :string(2)
#  locality    :string
#  pobox       :string
#  postal_code :string(16)
#  region      :string
#  street      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

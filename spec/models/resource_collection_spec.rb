require 'rails_helper'

RSpec.describe ResourceCollection, type: :model do
  let(:rcol) { Fabricate(:resource_collection) }
  
  describe 'factory' do
    it 'exists and is valid' do
      expect(rcol).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to a Resource' do
      expect(rcol).to belong_to(:resource)
    end
    it 'belongs to a Collection' do
      expect(rcol).to belong_to(:collection)
    end
  end

  describe 'validations' do
    describe '#collection' do
      it 'must be present' do
        expect(rcol.collection).not_to be_nil
      end
    end
    describe '#resource' do
      it 'must be present' do
        expect(rcol.resource).not_to be_nil
      end
    end
  end
end

# == Schema Information
#
# Table name: resource_collections
#
#  id            :bigint(8)        not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  collection_id :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_collections_on_collection_id  (collection_id)
#  index_resource_collections_on_resource_id    (resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (collection_id => collections.id)
#  fk_rails_...  (resource_id => resources.id)
#

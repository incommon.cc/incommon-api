require 'rails_helper'

RSpec.describe Map, type: :model do
  let(:map) { Fabricate(:map) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(map).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to an Agent' do
      expect(map).to belong_to(:agent)
    end
    it 'belongs to a Collection' do
      expect(map).to belong_to(:collection)
    end
    it 'belongs to a Position' do
      expect(map).to belong_to(:position)
    end
    it 'belongs to a Taxonomy' do
      expect(map).to belong_to(:taxonomy)
    end
    it 'has many ResourceCollections' do
      expect(map).to have_many(:resource_collections)
    end
    it 'has many Resources' do
      expect(map).to have_many(:resources)
    end
  end

  describe 'validations' do
    describe '#uuid' do
      # This is taken care of by UUIDParameter
      it 'is present' do
        expect(map).to respond_to(:uuid)
      end
    end
    describe '#zoom' do
      it 'must be present' do
        map.zoom = nil
        expect(map).not_to be_valid
      end
      it 'must be between 1 and 18' do
        map.zoom = 0
        expect(map).not_to be_valid
        map.zoom = 1
        expect(map).to be_valid
        map.zoom = 18
        expect(map).to be_valid
        map.zoom = 19
        expect(map).not_to be_valid
      end
    end
  end
  describe '#center' do
    it 'is the center of associated Position' do
      expect(map.center).to eql([map.position.longitude.to_f, map.position.latitude.to_f])
    end
  end
end

# == Schema Information
#
# Table name: maps
#
#  id            :bigint(8)        not null, primary key
#  uuid          :uuid
#  zoom          :integer          default(13)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  agent_id      :bigint(8)
#  collection_id :bigint(8)
#  position_id   :bigint(8)
#  taxonomy_id   :bigint(8)
#
# Indexes
#
#  index_maps_on_collection_id  (collection_id)
#  index_maps_on_position_id    (position_id)
#  index_maps_on_taxonomy_id    (taxonomy_id)
#  index_maps_on_uuid           (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (collection_id => collections.id)
#  fk_rails_...  (position_id => positions.id)
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#

require 'rails_helper'

RSpec.describe Position, type: :model do
  let(:pos) { Fabricate(:position) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(pos).to be_valid
    end
  end

  describe 'associations' do
    it 'has many :locations' do
      expect(pos).to have_many(:locations)
    end
    context 'through locations' do
      it 'has many :agents' do
        expect(pos).to have_many(:agents)
      end
      it 'has many :entities' do
        expect(pos).to have_many(:entities)
      end
      it 'has many :places' do
        expect(pos).to have_many(:places)
      end
      it 'has many :services' do
        expect(pos).to have_many(:services)
      end
      it 'has many :things' do
        expect(pos).to have_many(:things)
      end
    end
  end

  describe 'validations' do
    describe '#box' do
      it 'is optional' do
        expect(pos.box).to be_nil
      end
      context 'when present' do
        it 'covers maximum height on Earth' do
          # Mount Everest (Chimborazo is higher from Earth's center, but
          # elevation is from sea level)
          pos.elevation = 8848.00
          expect(pos).to be_valid
        end
      end
    end
    describe '#geo_type' do
      it 'is required' do
        expect(pos.geo_type).to be_present
      end
      it 'is a GeoJSON type' do
        expect(Position::GEO_TYPES).to include('Point')
        expect(Position::GEO_TYPES).to include('MultiPoint')
        expect(Position::GEO_TYPES).to include('LineString')
        expect(Position::GEO_TYPES).to include('MultiLineString')
        expect(Position::GEO_TYPES).to include('Polygon')
        expect(Position::GEO_TYPES).to include('MultiPolygon')
        expect(Position::GEO_TYPES).to include(pos.geo_type)
      end
    end
    describe '#geometry' do
      it 'is required' do
        expect(pos.geometry).to be_present
      end
      it 'is a GeoJSON feature geometry' do
        expect(pos.geometry.class.ancestors).to include(RGeo::Feature::Instance)
      end
    end
    describe '#latitude' do
      pending 'is optional'
      context 'when a Point' do
        it 'is required' do
          expect(pos.geo_type).to eql('Point')
          expect(pos.latitude).to be_present
        end
      end
      context 'when present' do
        it 'is comprised between -90 and +90' do
          expect(-90..90).to cover(pos.latitude)
          pos.latitude = 90.0000001
          expect(pos).not_to be_valid
        end
        it 'has precision 9, scale 7' do
          lat = 12.3456789012345678
          pos.geometry = "POINT (#{pos.longitude} #{lat})"
          pos.save
          expect(pos.latitude).to eql(0.123456789e2)
        end
        pending 'matches the center of an area'
      end
    end
    describe '#longitude' do
      pending 'is optional'
      context 'when a Point' do
        it 'is required' do
          expect(pos.geo_type).to eql('Point')
          expect(pos.longitude).to be_present
        end
      end
      context 'when present' do
        it 'is comprised between -180 and +180' do
          expect(-180..180).to cover(pos.longitude)
          pos.longitude = 180.0000001
          expect(pos).not_to be_valid
        end
        it 'has precision 7, scale 10' do
          lon = 123.45678901234567
          pos.geometry = "POINT (#{lon} #{pos.latitude})"
          pos.save
          expect(pos.longitude).to eql(0.1234567890e3)
        end
        pending 'matches the center of an area'
      end
    end
    describe '#radius' do
      it 'is optional' do
        expect(pos.radius).to be_nil
      end
      context 'when present' do
        before { pos.radius = 500 }
        it 'is positive' do
          expect(pos).to be_valid
          pos.radius = -1
          expect(pos).not_to be_valid
        end
        pending 'describes a circle around a Point'
        pending 'is in meters'
      end
    end
  end
end

# == Schema Information
#
# Table name: positions
#
#  id         :bigint(8)        not null, primary key
#  box        :json
#  elevation  :decimal(7, 2)
#  geo_type   :string
#  geometry   :geometry({:srid= geometry, 0
#  latitude   :decimal(9, 7)
#  longitude  :decimal(10, 7)
#  radius     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_positions_on_geometry  (geometry) USING gist
#

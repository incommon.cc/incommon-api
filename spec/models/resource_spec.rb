require 'rails_helper'

RSpec.describe Resource, type: :model do
  let(:res) { Fabricate(:resource) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(res).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to an Agent' do
      expect(res).to belong_to(:agent).optional
    end
  end

  describe 'validations' do
    pending '#meta'
    pending '#visible'
    pending '#translations'
    describe '#type' do
      it 'defaults to \'Resource\'' do
        r = Resource.new(name: 'Sample')
        expect(r).to receive(:save).and_return(true)
        r.save
        expect(r.type).to eql('Resource')
      end
    end
    describe '#uuid' do
      it 'is optional' do
        res.uuid = nil
        expect(res).to be_valid
      end
      it 'is managed by UUIDParameter' do
        expect(res.class.ancestors).to include(UUIDParameter)
      end
    end
  end
end

# == Schema Information
#
# Table name: resources
#
#  id           :bigint(8)        not null, primary key
#  meta         :json
#  translations :json
#  type         :string(16)       default("Resource")
#  uuid         :uuid
#  visible      :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_resources_on_agent_id  (agent_id)
#

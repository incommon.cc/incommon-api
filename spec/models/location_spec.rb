require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:location) { Fabricate(:location) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(location).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to a position' do
      expect(location).to belong_to(:position)
    end
    it 'belongs to a polymorphic locatable' do
      expect(location).to belong_to(:locatable)
    end
  end

  describe 'validations' do
    it 'requires a locatable' do
      location.locatable_type = nil
      location.locatable_id   = nil
      expect(location).not_to be_valid
    end
    it 'requires a position' do
      location.position_id = nil
      expect(location).not_to be_valid
    end
  end
end

# == Schema Information
#
# Table name: locations
#
#  id             :bigint(8)        not null, primary key
#  locatable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  locatable_id   :bigint(8)
#  position_id    :bigint(8)
#
# Indexes
#
#  index_locations_on_locatable_type_and_locatable_id  (locatable_type,locatable_id)
#  index_locations_on_position_id                      (position_id)
#
# Foreign Keys
#
#  fk_rails_...  (position_id => positions.id)
#

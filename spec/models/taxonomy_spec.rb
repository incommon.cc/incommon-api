# frozen_string_literal: true

RSpec.describe Taxonomy, type: :model do
  let(:taxonomy) { Fabricate(:taxonomy) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(taxonomy).to be_valid
    end
  end

  describe 'validations' do
    describe '#name' do
      it 'must be present' do
        taxonomy.name = nil
        expect(taxonomy).not_to be_valid
        expect(taxonomy.errors[:name]).to be_present
      end
      it 'must have between 3 and 64 characters' do
        taxonomy.name = 'a' * 2
        expect(taxonomy).not_to be_valid
        taxonomy.name << 'a'
        expect(taxonomy).to be_valid
        taxonomy.name = 'a' * 64
        expect(taxonomy).to be_valid
        taxonomy.name << 'a'
        expect(taxonomy).not_to be_valid
      end
      it 'must be unique' do
        expect { Fabricate(:taxonomy, name: taxonomy.name) }
          .to raise_error(ActiveRecord::RecordInvalid)
                .with_message("Validation failed: Name has already been taken")
      end
    end
    describe '#uuid' do
      it 'must be present and is automatically created if passed nil' do
        taxonomy.uuid = nil
        # UUIDParameter ensures this does not happen:
        # expect(taxonomy).not_to be_valid
        # expect(taxonomy.errors[:uuid]).to be_present
        # expect(taxonomy.errors.details[:uuid].first[:error]).to be "must be a valid random UUID (v4)"
        expect(taxonomy.save).to be true
        expect(taxonomy.uuid).not_to be_nil
      end
      it 'cannot be changed' do
        original_uuid = taxonomy.uuid
        taxonomy.uuid = SecureRandom.uuid
        # UUIDParameter guards from changes
        expect(taxonomy.save).to be_truthy
        expect(taxonomy.reload.uuid).to eq(original_uuid)
      end
      it 'conforms to UUID v4 (random) specification' do
        t_with_v4_uuid = Fabricate(:taxonomy, uuid:'abcdef01-2345-4789-abcd-ef0123456789')
        #                           Random UUID version 4 is here ^
        expect(t_with_v4_uuid).to be_valid
      end
      it 'rejects UUIDs with another version' do
        [
          'abcdef01-2345-0789-abcd-ef0123456789',
          'abcdef01-2345-1789-abcd-ef0123456789',
          'abcdef01-2345-2789-abcd-ef0123456789',
          'abcdef01-2345-3789-abcd-ef0123456789',
          'abcdef01-2345-5789-abcd-ef0123456789'
        ].each do
          expect { Fabricate(:taxonomy, uuid:'abcdef01-2345-0789-abcd-ef0123456789') }
            .to raise_error(ActiveRecord::RecordInvalid)
                  .with_message("Validation failed: Uuid must be a random UUID (v4)")
        end
      end
    end
  end
end

# == Schema Information
#
# Table name: taxonomies
#
#  id               :bigint(8)        not null, primary key
#  categories_count :integer          default(0)
#  translations     :json
#  uuid             :string(36)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  agent_id         :bigint(8)
#
# Indexes
#
#  index_taxonomies_on_agent_id  (agent_id)
#  index_taxonomies_on_uuid      (uuid) UNIQUE
#

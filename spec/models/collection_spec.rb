require 'rails_helper'

RSpec.describe Collection, type: :model do
  let(:collection) { Fabricate(:collection) }
  
  describe 'factory' do
    it 'exists and is valid' do
      expect(collection).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to an Agent' do
      expect(collection).to belong_to(:agent)
    end
    it 'has many ResourceCollections' do
      expect(collection).to have_many(:resource_collections)
    end
    it 'has many Maps' do
      expect(collection).to have_many(:maps)
    end
  end

  describe 'validations' do
    describe '#name' do
      it 'must be present' do
        collection.name= nil
        expect(collection).not_to be_valid
      end
      it 'must be unique within the scope of its Agent' do
        expect do
          Fabricate(:collection,
                    name: collection.name,
                    agent: collection.agent)
        end.to raise_error(ActiveRecord::RecordInvalid)
                 .with_message("Validation failed: Name has already been taken")
      end
      pending 'is translated'
    end
    describe '#summary' do
      it 'is optional' do
        collection.summary = nil
        expect(collection).to be_valid
      end
      it 'is limited to 160 characters' do
        collection.summary = 'a' * 160
        expect(collection).to be_valid
        collection.summary += 'b'
        expect(collection).not_to be_valid
      end
      pending 'is translated'
    end
    describe '#description' do
      it 'is optional' do
        collection.description = nil
        expect(collection).to be_valid
      end
      pending 'is Markdown'
      pending 'is translated'
    end
  end
end
# == Schema Information
#
# Table name: collections
#
#  id           :bigint(8)        not null, primary key
#  translations :json
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_collections_on_agent_id  (agent_id)
#

# == Schema Information
#
# Table name: collections
#
#  id           :bigint(8)        not null, primary key
#  translations :json
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_collections_on_agent_id  (agent_id)
#

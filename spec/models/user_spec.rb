require 'rails_helper'

RSpec.describe User, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
#
# Table name: users
#
#  id              :bigint(8)        not null, primary key
#  api_token       :uuid
#  auth_token      :string
#  password_digest :string
#  sha256_hash     :string(64)
#  token           :string
#  username        :string
#  uuid            :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

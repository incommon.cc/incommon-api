require 'rails_helper'

RSpec.describe Phone, type: :model do
  let(:phone) { Fabricate(:phone) }

  describe 'factory' do
    it 'exists and is valid' do
      expect(phone).to be_valid
    end
  end

  describe 'validations' do
    describe '#number' do
      it 'must be present' do
        phone.number = nil
        expect(phone).not_to be_valid
      end
      pending 'is a valid E.164 international number'
    end
  end
end

# == Schema Information
#
# Table name: phones
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  number     :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_phones_on_number  (number) UNIQUE
#

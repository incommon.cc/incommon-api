require 'rails_helper'

RSpec.describe Encryptable do
  context 'when included' do
    pending 'sets @@encrypted_columns to []'
    pending 'accepts any columns'
    pending 'raises error on unknown columns'
    pending 'can be called more than once'
    pending 'defines a setter to encrypt value automatically'
    pending 'defines a getter to read cleartext form of the value'
    pending 'does not affect model validation process'
  end
end

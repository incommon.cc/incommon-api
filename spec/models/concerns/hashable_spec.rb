# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hashable do
  let(:email) { Fabricate(:email) }
  
  it 'responds to :hexdigest_hashable_columns' do
    expect(email).to respond_to(:hexdigest_hashable_columns)
  end
  it 'hashes values to SHA256 separated with |' do
    expect(email.hexdigest_hashable_columns)
      .to eql(Digest::SHA256.hexdigest("#{email.address}"))
  end
end

require 'rails_helper'

RSpec.describe Role, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
#
# Table name: roles
#
#  id         :bigint(8)        not null, primary key
#  roles      :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  agent_id   :bigint(8)
#  user_id    :bigint(8)
#
# Indexes
#
#  index_roles_on_agent_id              (agent_id)
#  index_roles_on_user_id               (user_id)
#  index_roles_on_user_id_and_agent_id  (user_id,agent_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

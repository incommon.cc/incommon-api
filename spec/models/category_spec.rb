require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:category) { Fabricate(:category) }
  
  describe 'factory' do
    it 'exists and is valid' do
      expect(category).to be_valid
    end
  end

  describe 'associations' do
    it 'belongs to a Taxonomy' do
      expect(category).to belong_to(:taxonomy)
    end
    it 'has many Sections' do
      expect(category).to have_many(:sections)
    end
  end

  describe 'validations' do
    describe '#name' do
      it 'must be present' do
        category.name = nil
        expect(category).not_to be_valid
      end
      it 'must be unique within the scope of its Taxonomy' do
        expect do
          Fabricate(:category,
                    name: category.name,
                    taxonomy: category.taxonomy)
        end.to raise_error(ActiveRecord::RecordInvalid)
                         .with_message("Validation failed: Name has already been taken")
      end
    end
  end
end

# == Schema Information
#
# Table name: categories
#
#  id             :bigint(8)        not null, primary key
#  color          :string(25)
#  rank           :integer
#  sections_count :integer          default(0)
#  translations   :json
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  taxonomy_id    :bigint(8)
#
# Indexes
#
#  index_categories_on_taxonomy_id  (taxonomy_id)
#
# Foreign Keys
#
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#

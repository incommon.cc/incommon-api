require 'rails_helper'

RSpec.describe UserEmail, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
#
# Table name: user_emails
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email_id   :bigint(8)
#  user_id    :bigint(8)
#
# Indexes
#
#  index_user_emails_on_email_id  (email_id)
#  index_user_emails_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (email_id => emails.id)
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe Link, type: :model do
  let (:link) { Fabricate(:link) }
  
  describe 'factory' do
    it 'exists and is valid' do
      expect(link).to be_valid
    end
  end

  describe 'validations' do
    describe '#uri' do
      it 'must be present' do
        link.uri = nil
        expect(link).not_to be_valid
      end
      it 'must be a valid URI' do
        expect(link.uri).to match(URI::ABS_URI)
      end
    end
    describe '#flags' do
      it 'has a bitfield :secure' do
        expect(link).to have_a_bitfield :secure
      end
      it 'has the secure flag for https links' do
        link.uri = 'https://example.org'
        link.save
        expect(link).to be_secure
      end
      it 'has the secure flag for ircs links' do
        link.uri = 'ircs://irc.freenode.net#incommon-api'
        link.save
        expect(link).to be_secure
      end
      it 'has the secure flag for nntps links' do
        link.uri = 'nntps://example.org'
        link.save
        expect(link).to be_secure
      end
      it 'has not the secure flag for insecure schemes' do
        %w(http://example.org
           http://some.other.example.net/with/a/path/and?args
           nntp:news.example.net
           irc://irc.freenode.net#incommon-api
           gopher:one.upon.a.time
           weird:example.net).each do |uri|                             
          link.uri = uri
          link.save
          expect(link).to be_insecure
        end
      end
    end
  end
end

# == Schema Information
#
# Table name: links
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  uri        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

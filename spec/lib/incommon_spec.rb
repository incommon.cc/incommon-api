require 'rails_helper'

RSpec.describe 'INCOMMON Library' do
  describe '#rails_in_production?' do
    it 'returns false when not in production' do
      expect(INCOMMON.rails_in_production?).to be false
    end
    it 'returns false when Rails is not defined' do
      expect(ENV.fetch('NON-EXISTENT-ENV-VAR', 'no_rails')).to eql('no_rails')
      expect('no_rails' == :production).to be false
    end
  end
end

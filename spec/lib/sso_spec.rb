require 'rails_helper'

describe 'SSO with Discourse' do
  describe 'Class' do
    subject { SSO::FromDiscourse }

    it 'responds to #config' do
      expect(subject.respond_to?(:config)).to be true
    end
  end

  describe 'Class config' do
    subject { SSO::FromDiscourse.config } 

    it 'is present (in config/initializers/sso_config.rb)' do
      expect(subject).not_to be_nil
      expect(subject).to be_kind_of Hash
    end
    it 'sets :sso_url to Discourse SSO provider' do
      expect(subject[:sso_url]).not_to be_blank
    end
    it 'sets :return_url to /authenticate locally' do
      expect(subject[:return_url]).to eq("#{Rails.configuration.x.incommon.api_root_url}/authenticate")
    end
    it 'sets :sso_secret from Rails credentials' do
      expect(Rails.application.credentials.sso_secret).not_to be_nil
      expect(subject[:sso_secret]).to eq(Rails.application.credentials.sso_secret)
    end
  end

  describe 'Instance' do
    subject { SSO::FromDiscourse.new }

    describe '#config' do
      it 'is a private method' do
        subject.config
      rescue NoMethodError => e
        expect(e.message).to start_with("private method `config' called")
      end
      it 'responds with Class config' do
        expect(subject.__send__(:config)).to eq(subject.class.config)
      end
    end
    
    it 'responds to #nonce with a nonce' do
      expect(subject.respond_to?(:nonce)).to be true
      expect(subject.nonce).to match(%r{\A[a-f0-9]{32}\z})
    end
    it 'responds to #token with a token' do
      expect(subject.respond_to?(:token)).to be true
      expect(subject.token).to match(%r{\A[a-f0-9]{32}\z})
    end
    it 'gives a token different to the nonce' do
      expect(subject.token).not_to eq(subject.nonce)
    end
    it 'has nil :user_info' do
      expect(subject.user_info).to be_nil
    end
    it 'has :unauthorized status' do
      expect(subject.status).to eq(:unauthorized)
    end
  end

  context 'with a nonce' do
    let(:nonce) { SecureRandom.hex(16) }
    subject { SSO::FromDiscourse.new(nonce: nonce) }

    it 'accepts given nonce' do
      expect(subject.nonce).to eq(nonce)
    end
  end

  context 'with a token' do
    let(:token) { SecureRandom.hex(16) }
    subject { SSO::FromDiscourse.new(token: token) }

    it 'accepts given token' do
      expect(subject.token).to eq(token)
    end
  end

  describe '#parse' do
    # TODO: mock SSO response from Discourse
    subject { SSO::FromDiscourse.new }

    context 'failure' do
      it 'raises ArgumentError on invalid HMAC signature'
      it 'raises ArgumentError on HMAC signature mismatch'
      it 'raises ArgumentError on invalid encoding'
      it 'raises ArgumentError on nonce mismatch'
    end

    context 'success' do
      it 'requires a properly formed signature'
      it 'requires signature to match sso param'
      it 'requires sso param to be Base64-encoded'
      it 'must match nonce encoded in sso response'
      it 'sets @user_info from sso response'
      it 'sets @status to :ok'
    end
  end
end
